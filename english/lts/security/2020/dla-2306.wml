<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there was an escaping issue in
libphp-phpmailer, an email generation utility class for the PHP
programming language.</p>

<p>The `Content-Type` and `Content-Disposition` headers could have
permitted file attachments that bypassed attachment filters which
match on filename extensions.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
5.2.14+dfsg-2.3+deb9u2.</p>

<p>We recommend that you upgrade your libphp-phpmailer packages.</p>

<p>For the detailed security status of libphp-phpmailer please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libphp-phpmailer">https://security-tracker.debian.org/tracker/libphp-phpmailer</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2306.data"
# $Id: $
