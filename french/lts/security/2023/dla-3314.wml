#use wml::debian::translation-check translation="ae5e40af276882588a37de287f21da1ef6c6f7b9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans SDL2, la
bibliothèque Simple DirectMedia Layer. Ces vulnérabilités pouvaient permettre à
un attaquant de provoquer un déni de service ou aboutir dans l'exécution de code
arbitraire si des fichiers d’image ou audio mal formés étaient traités.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.0.9+dfsg1-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libsdl2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libsdl2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libsdl2">\
https://security-tracker.debian.org/tracker/libsdl2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3314.data"
# $Id: $
