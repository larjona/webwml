<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential remote denial of service
vulnerability in Redis, a popular key-value database.</p>

<p>Authenticated users could have used string matching commands (like
<code>SCAN</code> or <code>KEYS</code>) with a specially crafted pattern to
trigger a denial-of-service attack, causing it to hang and consume 100% CPU
time.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36021">CVE-2022-36021</a>

    <p>Redis is an in-memory database that persists on disk. Authenticated users can use string matching commands (like `SCAN` or `KEYS`) with a specially crafted pattern to trigger a denial-of-service attack on Redis, causing it to hang and consume 100% CPU time. The problem is fixed in Redis versions 6.0.18, 6.2.11, 7.0.9.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
5:5.0.14-1+deb10u3.</p>

<p>We recommend that you upgrade your redis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3361.data"
# $Id: $
