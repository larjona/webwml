<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Li Fei found that libproxy, a library for automatic proxy configuration
management, was vulnerable to a buffer overflow vulnerability when
receiving a large PAC file from a server without a Content-Length header
in the response.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.4.14-2+deb9u2.</p>

<p>We recommend that you upgrade your libproxy packages.</p>

<p>For the detailed security status of libproxy please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libproxy">https://security-tracker.debian.org/tracker/libproxy</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2450.data"
# $Id: $
