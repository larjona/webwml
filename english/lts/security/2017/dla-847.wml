<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The TeX system allows for calling external programs from within the TeX
source code. This has been restricted to a small set of programs since a
long time ago.</p>

<p>Unfortunately it turned out that one program in the list, mpost, allows
in turn to specify other programs to be run, which allows arbitrary code
execution when compiling a TeX document.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2012.20120611-5+deb7u1.</p>

<p>We recommend that you upgrade your texlive-base packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-847.data"
# $Id: $
