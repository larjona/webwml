#use wml::debian::translation-check translation="0ed821cc4d0461d6551da9a36092809f2bb12659" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cristian-Alexandru Staicu a découvert une vulnérabilité de pollution de
prototype dans inode-cached-path-relative, un module de Node.js utilisé pour
cacher (stocker) le résultat de path.relative.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16472">CVE-2018-16472</a>

<p>Un attaquant contrôlant le chemin et la valeur en cache peut monter une
attaque de pollution de prototype et par conséquent écraser des propriétés
arbitraires dans Object.prototype, pouvant aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23518">CVE-2021-23518</a>

<p>Le correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-16472">CVE-2018-16472</a>
était incomplet et d’autres vulnérabilités de pollution de prototype ont été
trouvées depuis ce temps-là, conduisant à un nouveau CVE.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 1.0.1-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-cached-path-relative.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-cached-path-relative,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-cached-path-relative">\
https://security-tracker.debian.org/tracker/node-cached-path-relative</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3221.data"
# $Id: $
