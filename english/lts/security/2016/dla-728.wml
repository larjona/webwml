<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine, which may result in possible timing attacks to
determine valid user names, bypass of the SecurityManager, disclosure of
system properties, unrestricted access to global resources, arbitrary
file overwrites, and potentially escalation of privileges.</p>

<p>In addition this update further hardens Tomcat's init and maintainer
scripts to prevent possible privilege escalations. Thanks to Paul
Szabo for the report.</p>

<p>This is probably the last security update of Tomcat 6 which will reach
its end-of-life exactly in one month. We strongly recommend to switch
to another supported version such as Tomcat 7 at your earliest
convenience.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
6.0.45+dfsg-1~deb7u3.</p>

<p>We recommend that you upgrade your tomcat6 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-728.data"
# $Id: $
