<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>A flaw was discovered in ruby-kramdown, a fast, pure ruby, Markdown
parser and converter, which could result in unintended read access to
files or unintended embedded Ruby code execution when the &#123;::options /&#125;
extension is used together with the <q>template</q> option.</p>

<p>The update introduces a new option <q>forbidden_inline_options</q> to
restrict the options allowed with the &#123;::options /&#125; extension. By
default the <q>template</q> option is forbidden.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1.17.0-1+deb10u1.</p>

<p>We recommend that you upgrade your ruby-kramdown packages.</p>

<p>For the detailed security status of ruby-kramdown please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-kramdown">\
https://security-tracker.debian.org/tracker/ruby-kramdown</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4743.data"
# $Id: $
