<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential path-traversal
vulnerability in GruntJS, a multipurpose task runner and build
system tool. This could have been exploited via malicious symlinks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0436">CVE-2022-0436</a>

    <p>Path Traversal in GitHub repository gruntjs/grunt prior to 1.5.2.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1.0.1-8+deb10u3.</p>

<p>We recommend that you upgrade your grunt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3386.data"
# $Id: $
