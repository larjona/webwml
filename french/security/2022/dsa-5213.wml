#use wml::debian::translation-check translation="62a9347ab03f598f32fdf4026d71c301c4fe2962" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Julian Gilbey a découvert que schroot, un outil permettant aux
utilisateurs d'exécuter des commandes dans un <q>chroot</q> (environnement
fermé d'exécution), a des règles trop permissives sur le nom des chroots
ou des sessions, permettant un déni de service sur le service schroot pour
tous les utilisateurs qui peuvent lancer une session schroot.</p>

<p>Veuillez noter que les chroots et sessions existants sont vérifiés
pendant la mise à niveau et que la mise à niveau est interrompue si un nom
futur non valable est détecté.</p>

<p>Les chroots et sessions problématiques peuvent être vérifiés avant la
mise à niveau avec la commande suivante :</p>

<code>schroot --list --all | LC_ALL=C grep -vE '^[a-z]+:[a-zA-Z0-9][a-zA-Z0-9_.-]*$'</code>

<p>Consultez
<url "https://codeberg.org/shelter/reschroot/src/tag/release/reschroot-1.6.13/NEWS#L10-L41">
pour des instructions permettant de résoudre ce type de situation.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 1.6.10-12+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets schroot.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de schroot, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/schroot">\
https://security-tracker.debian.org/tracker/schroot</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5213.data"
# $Id: $
