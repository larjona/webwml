<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have recently been discovered in libssh2, a
client-side C library implementing the SSH2 protocol </p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3855">CVE-2019-3855</a>

    <p>An integer overflow flaw which could have lead to an out of bounds
    write was discovered in libssh2 in the way packets were read from the
    server. A remote attacker who compromised an SSH server could have
    been able to execute code on the client system when a user connected
    to the server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3856">CVE-2019-3856</a>

    <p>An integer overflow flaw, which could have lead to an out of bounds
    write, was discovered in libssh2 in the way keyboard prompt requests
    were parsed. A remote attacker who compromised an SSH server could have
    been able to execute code on the client system when a user connected
    to the server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3857">CVE-2019-3857</a>

    <p>An integer overflow flaw which could have lead to an out of bounds
    write was discovered in libssh2 in the way SSH_MSG_CHANNEL_REQUEST
    packets with an exit signal were parsed. A remote attacker who
    compromises an SSH server could have been able to execute code on the
    client system when a user connected to the server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3858">CVE-2019-3858</a>

    <p>An out of bounds read flaw was discovered in libssh2 when a specially
    crafted SFTP packet was received from the server. A remote attacker
    who compromised an SSH server could have been able to cause a Denial
    of Service or read data in the client memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3859">CVE-2019-3859</a>

    <p>An out of bounds read flaw was discovered in libssh2's
    _libssh2_packet_require and _libssh2_packet_requirev functions. A
    remote attacker who compromised an SSH server could have be able to
    cause a Denial of Service or read data in the client memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3860">CVE-2019-3860</a>

    <p>An out of bounds read flaw was discovered in libssh2 in the way SFTP
    packets with empty payloads were parsed. A remote attacker who
    compromised an SSH server could have be able to cause a Denial of
    Service or read data in the client memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3861">CVE-2019-3861</a>

    <p>An out of bounds read flaw was discovered in libssh2 in the way SSH
    packets with a padding length value greater than the packet length
    were parsed. A remote attacker who compromised a SSH server could
    have been able to cause a Denial of Service or read data in the
    client memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3862">CVE-2019-3862</a>

    <p>An out of bounds read flaw was discovered in libssh2 in the way
    SSH_MSG_CHANNEL_REQUEST packets with an exit status message and no
    payload were parsed. A remote attacker who compromised an SSH server
    could have been able to cause a Denial of Service or read data in the
    client memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3863">CVE-2019-3863</a>

    <p>A server could have sent multiple keyboard interactive response
    messages whose total length were greater than unsigned char max
    characters. This value was used as an index to copy memory causing
    an out of bounds memory write error.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.4.3-4.1+deb8u2.</p>

<p>We recommend that you upgrade your libssh2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1730.data"
# $Id: $
