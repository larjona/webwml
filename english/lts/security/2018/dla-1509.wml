<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability has been discovered in php5, a server-side,
HTML-embedded scripting language.  The Apache2 component allows XSS via
the body of a "Transfer-Encoding: chunked" request because of a defect
in request handling.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
5.6.38+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1509.data"
# $Id: $
