#use wml::debian::translation-check translation="528bc4e5d04ccadc8b122fc584d4ea0922382bba" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Gson, une bibliothèque Java qui peut être utilisée pour convertir des
objets Java dans leurs représentations JSON et vice versa, était vulnérable
à un défaut de désérialisation. Une application pouvait désérialiser des
données non fiables sans vérifier suffisamment la validité des données
résultantes, permettant à un attaquant de contrôler l'état ou le flux
de l'exécution. Cela pouvait conduire à un déni de service ou même à
l'exécution de code arbitraire.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 2.8.6-1+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libgoogle-gson-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libgoogle-gson-java,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libgoogle-gson-java">\
https://security-tracker.debian.org/tracker/libgoogle-gson-java</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5227.data"
# $Id: $
