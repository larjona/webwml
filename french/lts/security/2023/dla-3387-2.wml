#use wml::debian::translation-check translation="3f38fa618c51384eec6f5f50eeef4fed5fa8c893" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une régression a été signalée indiquant que le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2021-3802">CVE-2021-3802</a>
cassait les paires option/valeur de montage permises dans des listes
<q>allow</q>, par exemple, errors=remount-ro.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.8.1-4+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets udisks2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de udisks2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/udisks2">\
https://security-tracker.debian.org/tracker/udisks2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3387-2.data"
# $Id: $
