#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Grégoire Scano"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l'hyperviseur Xen.

Le projet « Common vulnérabilités et Exposures » (CVE) identifie les problèmes
suivants :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10013">CVE-2016-10013</a> (xsa-204)

<p>Xen ne gère pas correctement l'unique étape d'un SYSCALL lors de
l'émulation, ce qui peut conduire à une élévation des privilèges. La
vulnérabilité n'expose que les invités HVM x86 64 bits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10024">CVE-2016-10024</a> (xsa-202)

<p>Les invités PV pourraient être capables de masquer des interruptions
causant un déni de service.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.1.6.lts1-5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xen.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-783.data"
# $Id: $
