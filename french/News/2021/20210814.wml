<define-tag pagetitle>Publication de Debian 11 <q>Bullseye</q></define-tag>
<define-tag release_date>2021-08-14</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="7ce619292bb7825470d6cb611887d68f405623ff" maintainer="Jean-Pierre Giraud"


<p>Après deux ans, un mois et neuf jours de développement, le projet Debian est
fier d'annoncer sa nouvelle version stable n° 11 (nom de code <q>Bullseye</q>),
qui sera suivie pendant les cinq prochaines années grâce à l'effort combiné de
<a href="https://security-team.debian.org/">l'équipe de sécurité de Debian</a>
et de <a href="https://wiki.debian.org/LTS">l'équipe de gestion à long terme de
Debian</a>.
</p>

<p>
Debian 11 <q>Bullseye</q> propose de nombreuses applications et environnements
de bureau. Entre autres, elle inclut maintenant les environnements de bureau
suivants :
</p>
<ul>
<li>Gnome 3.38 ;</li>
<li>KDE Plasma 5.20 ;</li>
<li>LXDE 11 ;</li>
<li>LXQt 0.16 ;</li>
<li>MATE 1.24 ;</li>
<li>Xfce 4.16.</li>
</ul>


<p>Cette version contient plus de 11 294 nouveaux paquets pour un total de
59 551 paquets, avec un nombre significatif de paquets (9 519) marqués comme
« obsolètes » et supprimés. 42 821 paquets ont été mis à jour et 5 434
demeurent inchangés. 

</p>

<p>
<q>Bullseye</q> est notre première version à fournir un noyau Linux avec la
prise en charge du système de fichiers exFAT et qui l'utilise par défaut pour
monter les systèmes de fichiers exFAT. Par conséquent, il n'est plus nécessaire
d'utiliser l'implémentation du système de fichiers dans l'espace utilisateur
fournie par le paquet exfat-fuse. Les outils pour créer et vérifier un système
de fichiers exFAT sont fournis par le paquet exfatprogs.
</p>


<p>
La plupart des imprimantes récentes sont capables d'utiliser l'impression et la
numérisation sans pilote sans avoir besoin des pilotes spécifiques du
fabriquant (souvent non libres).

<q>Bullseye</q> introduit un nouveau paquet, ipp-usb, qui utilise le protocole
IPP-over-USB indépendant du fabriquant et pris en charge par beaucoup
d'imprimantes récentes. Cela permet à un périphérique USB d'être vu comme un
périphérique réseau. Le dorsal officiel SANE sans pilote est fourni par
sane-escl du paquet libsane1 qui utilise le protocole eSCL.
</p>

<p>
Dans <q>Bullseye</q>, systemd active par défaut sa fonctionnalité de journal
persistant avec un repli implicite sur un stockage volatil. Cela permet aux
utilisateurs qui ne dépendent pas d'une fonctionnalité particulière de
désinstaller les démons de journalisation traditionnels et de passer à
l'utilisation exclusive du journal de systemd.
</p>

<p>
L'équipe Debian Med a pris part à la lutte contre la COVID-19 en empaquetant
des paquets pour la recherche sur le virus au niveau de son séquençage et pour
combattre la pandémie avec les outils utilisés en épidémiologie. Ce travail va
continuer en mettant l'accent sur les outils d'apprentissage automatique dans
ces deux domaines. Le travail de l'équipe sur l'assurance qualité et
l'intégration continue est essentiel sur le plan de la cohérence et de la
reproductibilité des résultats indispensable dans le domaine scientifique.

Le mélange Debian Med possède une série d'applications cruciales pour leurs
performances qui bénéficient maintenant de SIMD Everywhere. Pour installer les
paquets entretenus par l'équipe Debian Med, il suffit d'installer les
métapaquets nommés med-* qui sont en version 3.6.x. 
</p>

<p>
Le chinois, le japonais, le coréen et d'autres langues bénéficient de la
nouvelle méthode de saisie Fcitx 5 qui succède à la méthode Fcitx 4, populaire
dans <q>Buster</q>. Cette nouvelle version prend bien mieux en charge les
extensions de Wayland (le gestionnaire d'affichage par défaut).
</p>

<p>
Debian 11 <q>Bullseye</q> inclut de nombreux paquets logiciels mis à jour (plus
de 72 % des paquets de la distribution précédente), par exemple :
</p>
<ul>
<li>Apache 2.4.48</li>
<li>Serveur DNS BIND 9.16</li>
<li>Calligra 3.2</li>
<li>Cryptsetup 2.3</li>
<li>Emacs 27.1</li>
<li>GIMP 2.10.22</li>
<li>Collection de compilateurs GNU 10.2</li>
<li>GnuPG 2.2.20</li>
<li>Inkscape 1.0.2</li>
<li>LibreOffice 7.0</li>
<li>Noyaux Linux série 5.10</li>
<li>MariaDB 10.5</li>
<li>OpenSSH 8.4p1</li>
<li>Perl 5.32</li>
<li>PHP7.4</li>
<li>PostgreSQL 13</li>
<li>Python 3, 3.9.1</li>
<li>Rustc 1.48</li>
<li>Samba 4.13</li>
<li>Vim 8.2</li>
<li>plus de 59 000 autres paquets prêts à l'emploi, construits à partir de plus
de 30 000 paquets source.</li>
</ul>

<p>
Avec cette large sélection de paquets, ainsi que sa traditionnelle gestion de
nombreuses architectures, Debian, une fois de plus, confirme son but d'être
le <q>système d'exploitation universel</q>. Elle est appropriée pour bien des cas
différents d'utilisation : des systèmes de bureau aux miniportables, des
serveurs de développement aux systèmes pour grappe, ainsi que des serveurs de
bases de données aux serveurs web ou de stockage. En même temps, des efforts
supplémentaires d'assurance qualité tels que des tests automatiques
d'installation et de mise à niveau pour tous les paquets de l'archive Debian
garantissent que <q>Bullseye</q> répond aux fortes attentes
de nos utilisateurs lors d'une publication stable de Debian.
</p>

<p>
Un total de neuf architectures sont gérées :
PC 64 bits/Intel EM64T/x86-64 (<code>amd64</code>),
PC 32 bits/Intel IA-32 (<code>i386</code>),
PowerPC 64 bits Motorola/IBM petit-boutiste (<code>ppc64el</code>),
IBM S/390 64 bits (<code>s390x</code>),
pour ARM, <code>armel</code> et <code>armhf</code> pour les anciens et
nouveaux matériels 32 bits plus <code>arm64</code> pour l'architecture
64 bits <q>AArch64</q>,
et pour MIPS, <code>mipsel</code> (petit-boutiste) pour les matériels 32 bits et
<code>mips64el</code> pour le matériel 64 bits petit-boutiste.
</p>

<h3>Vous voulez l'essayer ?</h3>
<p>
Si vous voulez simplement essayer Debian 11 <q>Bullseye</q> sans l'installer,
vous pouvez utiliser une des <a href="$(HOME)/CD/live/">images autonomes</a> « live »
qui chargent et exécutent le système d'exploitation complet, dans un mode en
lecture seule, dans la mémoire de votre ordinateur.
</p>

<p>
Ces images autonomes sont fournies pour les architectures <code>amd64</code> et
<code>i386</code> et sont disponibles pour des installations à l'aide de DVD,
clés USB ou d'amorçage par le réseau. Les utilisateurs peuvent choisir entre
plusieurs environnements de bureau : GNOME, KDE Plasma, LXDE, LXQt, MATE et Xfce.
Debian <q>Bullseye</q> autonome fournit une image autonome standard, ainsi, il
est possible d'essayer un système Debian de base sans interface utilisateur
graphique.
</p>
<p>
Si le système d'exploitation vous plaît, vous avez la possibilité de
l'installer sur le disque dur de votre machine à partir de l'image autonome.
Celle-ci comprend l'installateur indépendant Calamares ainsi que l'installateur
Debian standard. Davantage d'informations sont disponibles dans les
<a href="$(HOME)/releases/bullseye/releasenotes">notes de publication</a>
et sur la page de la <a href="$(HOME)/CD/live/">section des images
d'installation autonomes</a> du site de Debian.
</p>

<p>
Si vous souhaitez installer Debian 11 <q>Bullseye</q> directement sur le
disque dur de votre ordinateur, vous pouvez choisir parmi les nombreux supports
d'installation tels que les disques Blu-ray, CD et DVD et les clefs USB,
ainsi que par une connexion réseau. Plusieurs environnements de bureau
— Cinnamon, GNOME, KDE Plasma Desktop et Applications, LXDE, LXQt, MATE et
Xfce — peuvent être installés grâce à ces images. De plus, des disques
<q>multi-architectures</q> sont disponibles et permettent l'installation à
partir d'un choix entre plusieurs architectures à partir d'un seul disque. Vous
pouvez aussi toujours créer un média d'installation amorçable sur clef USB
(voir le <a href="$(HOME)/releases/bullseye/installmanual">manuel d'installation</a>
pour plus de détails).
</p>

<p>
Au sein de l'installateur Debian, beaucoup de développement a été réalisé
aboutissant à une amélioration de la prise en charge du matériel et à d'autres
nouvelles fonctionnalités.
</p>
<p>
Dans certains cas, une installation réussie peut encore présenter des problèmes
d'affichage lors du redémarrage dans le système installé ; pour ces cas, il y a
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">quelques solutions</a>
qui pourraient aider à se connecter quand même. Il existe aussi
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">une procédure basée sur isenkram</a>
qui permet aux utilisateurs de détecter et de corriger l'absence de microcode
dans leur système de façon automatique. Bien sûr, il faut peser le pour et le
contre de l'utilisation de cet outil dans la mesure où il est probable qu'il
nécessitera l'installation de paquets non libres.
</p>

<p>
En complément à cela, les 
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">\
images d'installation non libres qui incluent les paquets de microcode</a>
ont été améliorées pour qu'elles puissent anticiper le besoin de microcode sur
la machine installée (par exemple, pour les cartes graphiques AMD ou Nvidia ou
pour les dernières générations de matériel audio d'Intel).
</p>

<p>
Pour les utilisateurs de nuage, Debian propose la prise en charge directe de
beaucoup des plateformes de nuage bien connues. Les images officielles de
Debian sont faciles à sélectionner sur chaque offre d'image. Debian publie
également des <a href="https://cloud.debian.org/images/openstack/current/">images
OpenStack</a> toutes prêtes pour les architectures <code>amd64</code> et
<code>arm64</code>, prêtes à être téléchargées et utilisées dans des
configurations de nuage locales.
</p>

<p>
Debian peut maintenant être installée en 76 langues, dont la plupart sont
disponibles avec des interfaces utilisateur en mode texte et graphique.
</p>

<p>
Les images d'installation peuvent être téléchargées dès à présent au moyen de
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (le moyen recommandé),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> ou
<a href="$(HOME)/CD/http-ftp/">HTTP</a> ; consultez la page
<a href="$(HOME)/CD/">Debian sur CD</a> pour plus d'informations. <q>Bullseye</q>
sera également bientôt disponible sur DVD, CD et disques Blu-ray physiques
chez de nombreux <a href="$(HOME)/CD/vendors">distributeurs</a>.
</p>


<h3>Mise à niveau de Debian</h3>
<p>
La mise à niveau vers Debian 11 à partir de la version précédente, Debian 10
(nom de code <q>Buster</q>) est gérée automatiquement par l'outil de gestion de
paquets APT pour la plupart des configurations.
</p>

<p>
Pour <q>Bullseye</q>, la suite de sécurité s'appelle maintenant
bullseye-security et les utilisateurs devraient adapter leur fichier
de liste de sources d'APT en conséquence lors de la mise à niveau. Si votre
configuration d'APT comprend également l'épinglage ou la ligne
<code>APT::Default-Release</code>, il est vraisemblable qu'elle nécessite aussi
des adaptations. Consultez la section
<a href="https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information#security-archive">\
Changement de l’organisation de l’archive security</a> des notes de publication
pour plus de détails.
</p>

<p>
Si vous effectuez une mise à niveau à distance, n'oubliez pas de consulter la
section 
<a href="$(HOME)/releases/bullseye/amd64/release-notes/ch-information#ssh-not-available">\
Impossible d'établir de nouvelles connexions SSH durant la mise à niveau</a>. 
</p>

<p>
Comme toujours, les systèmes Debian peuvent être mis à niveau sans douleur, sur
place et sans période d'indisponibilité forcée, mais il est fortement
recommandé de lire les
<a href="$(HOME)/releases/bullseye/releasenotes">notes de publication</a> ainsi
que le <a href="$(HOME)/releases/bullseye/installmanual">manuel d'installation</a>
pour d'éventuels problèmes et pour des instructions détaillées sur
l'installation et la mise à niveau. Les notes de publications seront améliorées
et traduites dans les semaines suivant la publication.
</p>


<h2>À propos de Debian</h2>

<p>
Debian est un système d'exploitation libre, développé par plusieurs milliers
de volontaires provenant du monde entier qui collaborent à l'aide d'Internet.
Les points forts du projet sont l'implication basée sur le volontariat,
l'engagement dans le contrat social de Debian et le logiciel libre, ainsi que
son attachement à fournir le meilleur système d'exploitation possible. Cette
nouvelle version représente une nouvelle étape importante dans ce sens.
</p>


<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian à l'adresse <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez
un courrier électronique à &lt;press@debian.org&gt;.
</p>
