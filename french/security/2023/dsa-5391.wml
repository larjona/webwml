#use wml::debian::translation-check translation="d5c998b321d0385e5dde3efc7db84ef23d58fc5e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans libxml2, une
bibliothèque fournissant la gestion de la lecture, de la modification et de
l'écriture de fichiers XML et HTML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28484">CVE-2023-28484</a>

<p>Un défaut de déréférencement de pointeur NULL lors de l'analyse de
schémas XML non valables peut avoir pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29469">CVE-2023-29469</a>

<p>Il a été signalé que lors du hachage de chaînes vides non terminées par
un caractère NULL, xmlDictComputeFastKey pouvait produire des résultats
incohérents, ce qui pouvait conduire à diverses erreurs de logiques ou de
mémoire.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.9.10+dfsg-6.7+deb11u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxml2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxml2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libxml2">\
https://security-tracker.debian.org/tracker/libxml2</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5391.data"
# $Id: $
