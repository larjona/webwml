<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in clickhouse, a
column-oriented database system.</p>


<p>The vulnerabilities require authentication, but can be triggered by any user
with read permissions. This means the attacker must perform reconnaissance on
the specific ClickHouse server target to obtain valid credentials. Any set of
credentials would do, since even a user with the lowest privileges can trigger
all of the vulnerabilities. By triggering the vulnerabilities, an attacker can
crash the ClickHouse server, leak memory contents or even cause remote code
execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42387">CVE-2021-42387</a>:

  <p>Heap out-of-bounds read in Clickhouse's LZ4 compression codec when
  parsing a malicious query. As part of the LZ4::decompressImpl() loop,
  a 16-bit unsigned user-supplied value ('offset') is read from the
  compressed data. The offset is later used in the length of a copy
  operation, without checking the upper bounds of the source of the copy
  operation.</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42388">CVE-2021-42388</a>:

  <p>Heap out-of-bounds read in Clickhouse's LZ4 compression codec when
  parsing a malicious query. As part of the LZ4::decompressImpl() loop,
  a 16-bit unsigned user-supplied value ('offset') is read from the
  compressed data. The offset is later used in the length of a copy
  operation, without checking the lower bounds of the source of the copy
  operation.</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43304">CVE-2021-43304</a>:

  <p>Heap buffer overflow in Clickhouse's LZ4 compression codec when
  parsing a malicious query. There is no verification that the copy
  operations in the LZ4::decompressImpl loop and especially the
  arbitrary copy operation wildCopy&lt;copy_amount&gt;(op, ip,
  copy_end), don&amp;#8217;t exceed the destination buffer&amp;#8217;s
  limits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43305">CVE-2021-43305</a>:

  <p>Heap buffer overflow in Clickhouse's LZ4 compression codec when
  parsing a malicious query. There is no verification that the copy
  operations in the LZ4::decompressImpl loop and especially the
  arbitrary copy operation wildCopy&lt;copy_amount&gt;(op, ip,
  copy_end), don&amp;#8217;t exceed the destination buffer&amp;#8217;s
  limits. This issue is very similar to <a href="https://security-tracker.debian.org/tracker/CVE-2021-43304">CVE-2021-43304</a>, but the
  vulnerable copy operation is in a different wildCopy call.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
18.16.1+ds-4+deb10u1.</p>

<p>We recommend that you upgrade your clickhouse packages.</p>

<p>For the detailed security status of clickhouse please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/clickhouse">https://security-tracker.debian.org/tracker/clickhouse</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3176.data"
# $Id: $
