<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel
that may lead to a privilege escalation, denial of service or
information leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3702">CVE-2020-3702</a>

    <p>A flaw was found in the driver for Atheros IEEE 802.11n family of
    chipsets (ath9k) allowing information disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16119">CVE-2020-16119</a>

    <p>Hadar Manor reported a use-after-free in the DCCP protocol
    implementation in the Linux kernel. A local attacker can take
    advantage of this flaw to cause a denial of service or potentially
    to execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3653">CVE-2021-3653</a>

    <p>Maxim Levitsky discovered a vulnerability in the KVM hypervisor
    implementation for AMD processors in the Linux kernel: Missing
    validation of the `int_ctl` VMCB field could allow a malicious L1
    guest to enable AVIC support (Advanced Virtual Interrupt Controller)
    for the L2 guest. The L2 guest can take advantage of this flaw to
    write to a limited but still relatively large subset of the host
    physical memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3656">CVE-2021-3656</a>

    <p>Maxim Levitsky and Paolo Bonzini discovered a flaw in the KVM
    hypervisor implementation for AMD processors in the Linux kernel.
    Missing validation of the `virt_ext` VMCB field could allow a
    malicious L1 guest to disable both VMLOAD/VMSAVE intercepts and VLS
    (Virtual VMLOAD/VMSAVE) for the L2 guest. Under these circumstances,
    the L2 guest is able to run VMLOAD/VMSAVE unintercepted and thus
    read/write portions of the host's physical memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3679">CVE-2021-3679</a>

    <p>A flaw in the Linux kernel tracing module functionality could allow
    a privileged local user (with CAP_SYS_ADMIN capability) to cause a
    denial of service (resource starvation).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3732">CVE-2021-3732</a>

    <p>Alois Wohlschlager reported a flaw in the implementation of the
    overlayfs subsystem, allowing a local attacker with privileges to
    mount a filesystem to reveal files hidden in the original mount.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3739">CVE-2021-3739</a>

    <p>A NULL pointer dereference flaw was found in the btrfs filesystem,
    allowing a local attacker with CAP_SYS_ADMIN capabilities to cause a
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3743">CVE-2021-3743</a>

    <p>An out-of-bounds memory read was discovered in the Qualcomm IPC
    router protocol implementation, allowing to cause a denial of
    service or information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3753">CVE-2021-3753</a>

    <p>Minh Yuan reported a race condition in the vt_k_ioctl in
    drivers/tty/vt/vt_ioctl.c, which may cause an out of bounds
    read in vt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37576">CVE-2021-37576</a>

    <p>Alexey Kardashevskiy reported a buffer overflow in the KVM subsystem
    on the powerpc platform, which allows KVM guest OS users to cause
    memory corruption on the host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38160">CVE-2021-38160</a>

    <p>A flaw in the virtio_console was discovered allowing data corruption
    or data loss by an untrusted device.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38166">CVE-2021-38166</a>

    <p>An integer overflow flaw in the BPF subsystem could allow a local
    attacker to cause a denial of service or potentially the execution
    of arbitrary code. This flaw is mitigated by default in Debian as
    unprivileged calls to bpf() are disabled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38199">CVE-2021-38199</a>

    <p>Michael Wakabayashi reported a flaw in the NFSv4 client
    implementation, where incorrect connection setup ordering allows
    operations of a remote NFSv4 server to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40490">CVE-2021-40490</a>

    <p>A race condition was discovered in the ext4 subsystem when writing
    to an inline_data file while its xattrs are changing. This could
    result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41073">CVE-2021-41073</a>

    <p>Valentina Palmiotti discovered a flaw in io_uring allowing a local
    attacker to escalate privileges.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 5.10.46-5. This update includes fixes for #993948 and #993978.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4978.data"
# $Id: $
