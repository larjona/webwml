#use wml::debian::translation-check translation="9225eb268bc2759b8a0dc545bac4e292837c66fe" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité de lecture hors limites
et un dépassement d'entier par le bas dans open vSwitch, un commutateur virtuel
Ethernet à base logicielle.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4337">CVE-2022-4337</a>

<p>Lecture hors limites dans des TLV spécifiques à une organisation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4338">CVE-2022-4338</a>

<p>Dépassement d'entier par le bas dans des TLV spécifiques à une organisation.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.10.7+ds1-0+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openvswitch.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3253.data"
# $Id: $
