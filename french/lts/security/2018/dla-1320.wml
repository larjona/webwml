#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Samba, un serveur de
fichier SMB/CIFS, d'impression et de connexion pour Unix.

Le projet « Common vulnérabilités et Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1050">CVE-2018-1050</a>

<p>Samba est prédisposé à une attaque par déni de service lorsque le service de
spools RPC est configuré pour être exécuté comme un démon externe. Merci
à Jeremy Allison pour son correctif.</p>

<p><url "https://www.samba.org/samba/security/CVE-2018-1050.html"></p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.6.6-6+deb7u16.</p>

<p>Nous vous recommandons de mettre à jour vos paquets samba.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1320.data"
# $Id: $
