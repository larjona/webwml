#use wml::debian::translation-check translation="2e335f78374522778a0894b37e67b50229791f3c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités ont été découvertes dans le cadriciel de développement
web Django.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14232">CVE-2019-14232</a>

<p>Prévention d’un déni de service possible dans django.utils.text.Truncator.</p>

<p>Si l’argument html=True était passé aux méthodes chars() et words()
de django.utils.text.Truncator, elles étaient extrêmement lentes à évaluer
certaines entrées à cause d’une vulnérabilité de déni de service par expression
rationnelle. Les méthodes chars() et words() sont utilisées pour mettre en œuvre
les filtres de modèles truncatechars_html et truncatewords_html et étaient par
conséquent vulnérables.</p>

<p>Les expressions rationnelles utilisées par Truncator ont été simplifiées pour
éviter des problèmes de retour sur trace. Par conséquent, la ponctuation
terminale peut être parfois incluse dans la sortie tronquée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14233">CVE-2019-14233</a>

<p>Prévention d’un déni de service possible dans strip_tags().</p>

<p>À cause du comportement de HTMLParser sous-jacent,
django.utils.html.strip_tags() serait extrêmement lent à évaluer certaines
entrées contenant de longues séquences d’entités incomplètes HTML imbriquées. La
méthode strip_tags() est utilisée pour mettre en œuvre les filtres de modèle
« striptags » correspondants et était par conséquent aussi vulnérable.</p>

<p>Désormais strip_tags() évite les appels récursifs à HTMLParser lors de
l’avancement de la suppression de balises, sauf forcement les entités HTML
incomplètes, des arrêts étant faits.</p>

<p>Il faut se rappeler qu’absolument AUCUNE garantie n’est fournie pour que le
résultat de strip_tags() soit sûr du point de vue HTML. Aussi, ne JAMAIS marquer
comme sûr le résultat d’appel strip_tags() sans d’abord les protéger, par
exemple avec django.utils.html.escape().</p></li>
</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.7.11-1+deb8u7.</p>
<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1872.data"
# $Id: $
