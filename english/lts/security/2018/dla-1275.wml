<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the uwsgi_expand_path function in utils.c in
Unbit uWSGI, an application container server, has a stack-based buffer
overflow via a large directory length that can cause a
denial-of-service (application crash) or stack corruption.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.2.3+dfsg-5+deb7u2.</p>

<p>We recommend that you upgrade your uwsgi packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1275.data"
# $Id: $
