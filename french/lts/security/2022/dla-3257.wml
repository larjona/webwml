#use wml::debian::translation-check translation="f8cc0e2bbf25a70902ea15554b1d64fb0a1be18b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème dans Emacs par lequel des
attaquants pouvaient exécuter des commandes arbitraires à l’aide de
métacaractères dans le nom du fichier de code source.</p>

<p>Cela était provoqué parce que lib-src/etags.c utilisait la fonction system(3)
de bibliothèque lors de l’appel du binaire (externe) ctags(1).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-45939">CVE-2022-45939</a>

<p>GNU Emacs jusqu’à la version 28.2 permet à des attaquants d’exécuter des
commandes à l’aide de métacaractères d’interpréteur de commandes dans le nom
du fichier de code source, parce que lib-src/etags.c utilise la fonction de
bibliothèque C du système dans son implémentation du programme ctags. Par
exemple, une victime pouvait utiliser la commande <q>ctags *</q> (suggérée dans
la documentation de ctags) dans une situation où le répertoire de travail utilisé
avait du contenu qui dépendait d’une entrée non fiable.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:26.1+1-3.2+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets emacs.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3257.data"
# $Id: $
