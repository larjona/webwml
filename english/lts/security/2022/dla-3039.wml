<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Sebastian Krause discovered that manipulated inline images can force
PyPDF2, a pure Python PDF library, into an infinite loop, if a maliciously
crafted PDF file is processed.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.26.0-2+deb9u1.</p>

<p>We recommend that you upgrade your pypdf2 packages.</p>

<p>For the detailed security status of pypdf2 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/pypdf2">https://security-tracker.debian.org/tracker/pypdf2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3039.data"
# $Id: $
