#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Emilien Gaspar a découvert que collectd, un démon de surveillance et de
collecte de statistiques, traitait incorrectement les paquets réseau
entrants. Cela a pour conséquence un dépassement de zone mémoire,
permettant à un attaquant distant soit de provoquer un déni de service par
plantage de l'application, soit éventuellement d'exécuter du code
arbitraire.</p>

<p>En complément, des chercheurs en sécurité de l'Université de Columbia et
de l'Université de Virginie ont découvert que collectd échouait dans la
vérification de la valeur de retour durant l'initialisation. Cela signifie
que le démon pourrait parfois être lancé sans les paramètres de sécurité
souhaités.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 5.1.0-3+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets collectd.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-575.data"
# $Id: $
