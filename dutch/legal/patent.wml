#use wml::debian::template title="Standpunt van Debian over softwarepatenten" BARETITLE="true"
#use wml::debian::translation-check translation="088c528e5eb9d95504ef91aa449795178ac06be1"

<pre><code>Versie: 1.0
Gepubliceerd: 19 februari 2012
</code></pre>


<h1>Inleiding</h1>

<p>Dit document beschrijft het standpunt van Debian over patenten. Debian erkent de bedreiging die patenten vormen voor Vrije Software en blijft samenwerken met anderen in de Vrije Softwaregemeenschap bij het verweer inzake patenten.
</p>

<p>Het onderstaande beleid is bedoeld als een duidelijke en bruikbare leidraad voor leden van onze gemeenschap die geconfronteerd worden met patentkwesties, zodat de Debian-gemeenschap haar patentverweer kan coördineren zonder angst, onzekerheid en twijfel.
</p>


<h1>Beleidsverklaring</h1>

<ol>

<li><p>Debian zal niet willens en wetens software verspreiden waarop patenten rusten; Debian-medewerkers mogen geen software verpakken of verspreiden waarvan ze weten dat deze een patent schendt.
</p></li>

<li><p>Debian accepteert geen patentlicentie die niet strookt met het <a href="$(HOME)/social_contract">Sociaal Contract van Debian</a> of met de <a href="$(HOME)/social_contract#guidelines">Richtlijnen van Debian voor Vrije Software</a>.
</p></li>

<li><p>Tenzij uitingen met betrekking tot patenten vallen onder het verschoningsrecht van een advocaat, kunnen leden van de gemeenschap worden gedwongen om deze in een rechtszaak te overleggen. Ook kunnen publiekelijk geuite zorgen over patenten ongegrond blijken te zijn, maar in de tussentijd veel angst, onzekerheid en twijfel creëren. Gelieve u daarom te onthouden van het publiekelijk posten van bezorgdheden over patenten of van het bespreken van patenten los van de communicatie met juridische adviseurs, waarop het verschoningsrecht van de advocaat van toepassing is.
</p></li>

<li><p>Risicovolle situaties inzake patenten belangen de hele gemeenschap aan. Als u zich zorgen maakt over een specifiek patent, houd dit dan niet voor uzelf &mdash; breng een juridisch adviseur op de hoogte.
</p></li>

<li><p>Alle communicatie in verband met specifieke risicovolle situaties inzake patenten moet worden gericht aan <a href="mailto:patents@debian.org">patents@debian.org</a>, dat wordt beheerd volgens de geheimhoudingsregels die gelden voor contacten tussen advocaten en cliënten. Het risico zal worden geëvalueerd en de nodige antwoorden zullen rechtstreeks aan de betrokken partijen worden verstrekt.
</p></li>

</ol>


<h1>Contactinformatie</h1>

<p>Als u contact met ons wilt opnemen over specifieke patentrisico's in het Debian-archief, stuur dan een e-mail naar <a href="mailto:patents@debian.org">patents@debian.org</a> met vermelding van:
</p>

<ul>
<li>uw naam,</li>
<li>uw functie in Debian,</li>
<li>de naam van de betrokken pakketten of projecten,</li>
<li>de titel, het nummer en het rechtsgebied van het (de) betrokken patent(en),</li>
<li>een beschrijving van uw bezorgdheid.</li>
</ul>


<h1>Aanvullende informatie</h1>

<p>Voor meer informatie over patenten en de interactie met vrije-softwaredistributies raden we aan onze <a href="$(HOME)/reports/patent-faq">Community Distribution Patent Policy FAQ</a> (Faq over het beleid van gemeenschapsdistributies inzake patenten) te lezen, een document bedoeld om ontwikkelaars van Vrije Software, en in het bijzonder uitgevers van distributies, te informeren over de risico's van softwarepatenten.
</p>


<hr />

<p>Het Debian Project dankt het <a href="http://www.softwarefreedom.org">Software Freedom Law Center</a> (SFLC) voor zijn juridisch advies in deze zaken.
</p>

