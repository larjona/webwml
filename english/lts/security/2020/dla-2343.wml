<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A directory traversal vulnerability was discovered in Icinga Web 2, a
web interface for Icinga, which could result in the disclosure of files
readable by the process.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.4.1-1+deb9u1.</p>

<p>We recommend that you upgrade your icingaweb2 packages.</p>

<p>For the detailed security status of icingaweb2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/icingaweb2">https://security-tracker.debian.org/tracker/icingaweb2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2343.data"
# $Id: $
