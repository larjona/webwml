#use wml::debian::template title="Installeren van Debian via het internet" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"

#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

# Translator: $Author$
# Last update: $Date$

<p>Deze methode om Debian te installeren vereist een werkende
internetverbinding <em>tijdens</em> de installatie. Vergeleken met de andere
methodes zult u minder data downloaden omdat het proces is aangepast aan uw
eisen. Ethernet en draadloze verbindingen worden ondersteund. Interne
ISDN-kaarten worden helaas <em>niet</em> ondersteund.</p>

<p>Er zijn drie mogelijkheden voor installatie over het internet:</p>

<toc-display />
<div class="line">
<div class="item col50">
<toc-add-entry name="smallcd">Kleine cd's of USB-sticks</toc-add-entry>

<p>Hierna vindt u imagebestanden.
Selecteer de architectuur van uw processor uit de onderstaande lijst.</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>Zie voor meer gedetailleerde informatie: <a
href="../CD/netinst/">Netwerkinstallatie vanaf een minimale cd</a></p>

<div class="line">
<div class="item col50">
<toc-add-entry name="verysmall">Mini-cd's, flexibele USB-sticks,
etc.</toc-add-entry>

<p>U kunt een aantal kleine imagebestanden (geschikt voor USB-sticks en
vergelijkbare media) downloaden, deze daarna naar het medium wegschrijven
en vervolgens de installatie starten door uw systeem vanaf dat medium op te
starten.</p>

<p>Er is enige variatie tussen verschillende platformen in de ondersteuning
van een installatie vanaf de diverse kleine images.</p>

<p>Voor meer gedetailleerde informatie verwijzen wij naar de
<a href="$(HOME)/releases/stable/installmanual">installatiehandleiding voor
uw platform</a>, met name het hoofdstuk
<q>Systeeminstallatiemedia verkrijgen</q>.</p>

<p>Hier zijn de links naar de imagebestanden (zie het bestand MANIFEST
voor informatie):</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">
<toc-add-entry name="netboot">Netwerk-opstart</toc-add-entry>

<p>Voor deze methode richt u een TFTP- en een DHCP (of BOOTP, of RARP)-server
in waarmee de installatiemedia beschikbaar worden gemaakt voor machines op uw
lokale netwerk. Als het BIOS van uw clients dit ondersteunt, kunt u
vervolgens het Debian installatiesysteem opstarten vanaf het netwerk (met
gebruik van PXE en TFTP) en daarna Debian verder installeren, eveneens vanaf
het netwerk.</p>

<p>Merk op dat niet alle machines vanaf het netwerk kunnen worden opgestart.
Daarnaast wordt deze installatiemethode vanwege het extra werk dat ervoor
nodig is, niet aangeraden voor beginnende gebruikers.</p>

<p>Voor meer gedetailleerde informatie verwijzen wij naar de
<a href="$(HOME)/releases/stable/installmanual">installatiehandleiding voor
uw platform</a>, met name het hoofdstuk
<q>Bestanden voorbereiden voor opstarten via TFTP over het netwerk</q>.</p>

<p>Hier zijn de links naar de imagebestanden (zie het bestand MANIFEST
voor informatie):</p>

<stable-netboot-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
#
<div id="firmware_nonfree" class="important">
<p>
Als op uw systeem bepaalde hardware <strong>vereist dat niet-vrije firmware
geladen wordt</strong> samen met het stuurprogramma voor het apparaat, kunt u
een van de <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">tar-archieven met gangbare firmware-pakketten</a>
gebruiken of een <strong>niet-officieel</strong> image downloaden dat deze
<strong>niet-vrije</strong> firmware bevat. Instructies over het gebruik van
tar-archieven en algemene informatie over het laden van firmware tijdens een
installatie kunt u vinden in de
<a href="../releases/stable/amd64/ch06s04">Installatiehandleiding</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">niet-officiële installatie-images voor de release
<q>stable</q> die firmware bevatten</a>
</p>
</div>

