<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This DLA fixes a a problem parsing x509 certificates, an pickle integer
overflow, and some other minor issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-0772">CVE-2016-0772</a>

    <p>The smtplib library in CPython does not return an error when StartTLS fails,
    which might allow man-in-the-middle attackers to bypass the TLS protections by
    leveraging a network position between the client and the registry to block the
    StartTLS command, aka a "StartTLS stripping attack."</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5636">CVE-2016-5636</a>

    <p>Integer overflow in the get_data function in zipimport.c in CPython
    allows remote attackers to have unspecified impact via a negative data size
    value, which triggers a heap-based buffer overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5699">CVE-2016-5699</a>

    <p>CRLF injection vulnerability in the HTTPConnection.putheader function in
    urllib2 and urllib in CPython allows remote attackers to inject arbitrary HTTP
    headers via CRLF sequences in a URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20406">CVE-2018-20406</a>

    <p>Modules/_pickle.c has an integer overflow via a large LONG_BINPUT value
    that is mishandled during a <q>resize to twice the size</q> attempt. This issue
    might cause memory exhaustion, but is only relevant if the pickle format is
    used for serializing tens or hundreds of gigabytes of data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5010">CVE-2019-5010</a>

    <p>NULL pointer dereference using a specially crafted X509 certificate.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.4.2-1+deb8u2.</p>

<p>We recommend that you upgrade your python3.4 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1663.data"
# $Id: $
