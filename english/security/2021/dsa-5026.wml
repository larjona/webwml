<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the Mozilla Firefox
web browser, which could potentially result in the execution
of arbitrary code, information disclosure or spoofing.</p>

<p>Debian follows the extended support releases (ESR) of Firefox. Support
for the 78.x series has ended, so starting with this update we're now
following the 91.x releases.</p>

<p>Between 78.x and 91.x, Firefox has seen a number of feature updates. For
more information please refer to
<a href="https://www.mozilla.org/en-US/firefox/91.0esr/releasenotes/">\
https://www.mozilla.org/en-US/firefox/91.0esr/releasenotes/</a></p>

<p>For the oldstable distribution (buster) one more final toolchain update
is needed, updated packages will shortly be available as 91.4.1esr-1~deb10u1</p>

<p>For the stable distribution (bullseye), these problem have been fixed in
version 91.4.1esr-1~deb11u1.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>For the detailed security status of firefox-esr please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5026.data"
# $Id: $
