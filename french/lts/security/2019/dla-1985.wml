#use wml::debian::translation-check translation="8cd6e5f96d36e25ffd8690c0c29a88960b8cc723" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème de déréférencement de pointeur
NULL dans l’encodeur et décodeur IW44 dans DjVu, un ensemble de technologies de
compression d’images de haute résolution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18804">CVE-2019-18804</a>

<p>DjVuLibre 3.5.27 possède un déréférencement de pointeur NULL dans la fonction
DJVU::filter_fv de IW44EncodeCodec.cpp.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.5.25.4-4+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets djvulibre.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1985.data"
# $Id: $
