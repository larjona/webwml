<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Zhuowei Zhang discovered a bug in the EAP authentication client code
of strongSwan, an IKE/IPsec suite, that may allow to bypass the client
and in some scenarios even the server authentication, or could lead to
a denial-of-service attack.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
5.5.1-4+deb9u6.</p>

<p>We recommend that you upgrade your strongswan packages.</p>

<p>For the detailed security status of strongswan please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/strongswan">https://security-tracker.debian.org/tracker/strongswan</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2909.data"
# $Id: $
