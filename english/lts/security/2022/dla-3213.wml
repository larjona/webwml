<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential Denial of Service (DoS) attack
against krb5, a suite of tools implementing the Kerberos authentication system.
An integer overflow in PAC parsing could have been exploited if a cross-realm
entity acted maliciously.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42898">CVE-2022-42898</a>

    <p>krb5_pac_parse() buffer parsing vulnerability</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1.17-3+deb10u5.</p>

<p>We recommend that you upgrade your krb5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3213.data"
# $Id: $
