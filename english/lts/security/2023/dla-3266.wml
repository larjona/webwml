<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were two issues in viewvc, a web-based
interface for browsing Subversion and CVS repositories. The attack vectors
involved files with unsafe names; names that, when embedded into an HTML
stream, could cause the browser to run unwanted code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-22456">CVE-2023-22456</a>

    <p>ViewVC, a browser interface for CVS and Subversion version control
    repositories, as a cross-site scripting vulnerability that affects versions
    prior to 1.2.2 and 1.1.29. The impact of this vulnerability is mitigated by
    the need for an attacker to have commit privileges to a Subversion
    repository exposed by an otherwise trusted ViewVC instance. The attack
    vector involves files with unsafe names (names that, when embedded into an
    HTML stream, would cause the browser to run unwanted code), which
    themselves can be challenging to create. Users should update to at least
    version 1.2.2 (if they are using a 1.2.x version of ViewVC) or 1.1.29 (if
    they are using a 1.1.x version). ViewVC 1.0.x is no longer supported, so
    users of that release lineage should implement a workaround. Users can edit
    their ViewVC EZT view templates to manually HTML-escape changed paths
    during rendering. Locate in your template set's `revision.ezt` file
    references to those changed paths, and wrap them with `[format "html"]` and
    `[end]`. For most users, that means that references to `[changes.path]`
    will become `[format "html"][changes.path][end]`. (This workaround should
    be reverted after upgrading to a patched version of ViewVC, else changed
    path names will be doubly escaped.)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-22464">CVE-2023-22464</a>

    <p>ViewVC is a browser interface for CVS and Subversion version control
    repositories. Versions prior to 1.2.3 and 1.1.30 are vulnerable to
    cross-site scripting. The impact of this vulnerability is mitigated by the
    need for an attacker to have commit privileges to a Subversion repository
    exposed by an otherwise trusted ViewVC instance. The attack vector involves
    files with unsafe names (names that, when embedded into an HTML stream,
    would cause the browser to run unwanted code), which themselves can be
    challenging to create. Users should update to at least version 1.2.3 (if
    they are using a 1.2.x version of ViewVC) or 1.1.30 (if they are using a
    1.1.x version). ViewVC 1.0.x is no longer supported, so users of that
    release lineage should implement one of the following workarounds. Users
    can edit their ViewVC EZT view templates to manually HTML-escape changed
    path "copyfrom paths" during rendering. Locate in your template set's
    `revision.ezt` file references to those changed paths, and wrap them with
    `[format "html"]` and `[end]`. For most users, that means that references
    to `[changes.copy_path]` will become `[format
    "html"][changes.copy_path][end]`. (This workaround should be reverted after
    upgrading to a patched version of ViewVC, else "copyfrom path" names will
    be doubly escaped.)</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
1.1.26-1+deb10u1.</p>

<p>We recommend that you upgrade your viewvc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3266.data"
# $Id: $
