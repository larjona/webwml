#use wml::debian::template title="Sistema de control de versiones del proyecto de documentación de Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="e6e987deba52309df5b062b1567c8f952d7bd476"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#access">Acceso a los fuentes en Git</a></li>
  <li><a href="#obtaining">Obtención de privilegios de actualización (de «push»)</a></li>
  <li><a href="#updates">Mecanismo de actualización automática</a></li>
</ul>


<aside>
<p><span class="fas fa-caret-right fa-3x"></span> El <a href="ddp">proyecto de documentación de Debian</a> (DDP, por sus siglas en inglés) almacena sus páginas web y la mayoría de los manuales en <a href="https://salsa.debian.org">Salsa</a>, la instancia GitLab de Debian. Cualquiera puede descargar los fuentes desde el servicio Salsa, pero solo los miembros del proyecto de documentación de Debian tienen acceso de escritura y pueden actualizar ficheros.</p>
</aside>

<h2><a id="access">Acceso a los fuentes en Git</a></h2>

<p>
El proyecto de documentación de Debian almacena la información en Salsa, la instancia GitLab de Debian. Para acceder a ficheros individuales y revisar modificaciones recientes o las actividades del proyecto en general, visite el <a href="https://salsa.debian.org/ddp-team/">repositorio del DDP</a>.
</p>

<p>
Si lo que desea es descargar manuales completos, disponer de acceso directo a Salsa es probablemente una opción mejor. Las secciones que siguen explican cómo clonar una repositorio Git (en modo de solo-lectura o de lectura-escritura) en su máquina local y cómo actualizar su copia local. Antes de empezar, por favor, instale el paquete <tt><a href="https://packages.debian.org/git">git</a></tt> en su máquina.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://wiki.debian.org/Salsa">Lea la documentación sobre Salsa</a></button></p>


<h3>Clonado de un repositorio Git de forma anónima (solo-lectura)</h3>

<p>
Use esta orden para descargar todos los ficheros de un proyecto:
</p>

<pre>
git clone https://salsa.debian.org/ddp-team/release-notes.git
</pre>

<p>
Haga lo mismo para cada proyecto que quiera descargar. <strong>Sugerencia:</strong> para obtener el URL correcto a especificar en la orden <code>git clone</code>, abra el proyecto en un navegador web, haga click en el botón azul (<em>Clone</em>) y copie el URL (<em>Clone with HTTPS</em>) al portapapeles.
</p>

<h3>Clonado de un repositorio Git con privilegios de actualización (lectura-escritura)</h3>

<p>
Para poder acceder al servidor Git de esta manera, antes le han tenido que conceder acceso de escritura al repositorio. Consulte <a href="#obtaining">esta sección</a> para más información sobre cómo solicitar permiso de actualización.
</p>

<p>
Con acceso de escritura a Salsa, puede emitir la orden siguiente para descargar todos los ficheros de un proyecto:
</p>

<pre>
git clone git@salsa.debian.org:ddp-team/release-notes.git
</pre> 

<p>Haga lo mismo para cada proyecto que quiera clonar.</p>

<h3>Descarga de modificaciones desde el repositorio Git remoto</h3>

<p>
Para actualizar su copia local con los cambios hechos por otros, entre en el subdirectorio del manual correspondiente y ejecute esta orden:
</p>

<pre>
git pull
</pre>

<h2><a id="obtaining">Obtención de privilegios de actualización (de «push»)</a></h2>

<p>
Cualquiera que desee participar en la escritura de manuales, FAQ, HOWTOs, etc., puede obtener privilegios de actualización (de «push»). Generalmente solo le pedimos que haya enviado previamente un par de parches útiles. Después de eso, siga estos pasos para solicitar acceso de escritura:
</p>

<ol>
  <li>Cree una cuenta en <a href="https://salsa.debian.org/">Salsa</a> si no dispone ya de una.</li>
  <li>Vaya al <a href="https://salsa.debian.org/ddp-team/">repositorio del DDP</a> y haga click en <em>Request Access</em>.</li>
  <li>Envíe un correo electrónico a <a href="mailto:debian-doc@lists.debian.org">debian-doc@lists.debian.org</a> y coméntenos cómo ha contribuido al proyecto Debian.</li>
  <li>Una vez que su solicitud haya sido aprobada, formará parte del equipo del DDP.</li>
</ol>

<h2><a id="updates">Mecanismo de actualización automática</a></h2>

<p>
Todos los manuales se publican como páginas web. Se generan automáticamente en www-master.debian.org como parte del proceso ordinario de generación del sitio web, que tiene lugar cada cuatro horas. Durante este proceso, se descarga desde el archivo la última versión de los paquetes, se recompila cada manual y se transfieren todos los ficheros al subdirectorio <code>doc/manuals/</code> del sitio web.
</p>

<p>Los ficheros de documentación generados por el script de actualización se encuentran en
<a href="manuals/">https://www.debian.org/doc/manuals/</a>.</p>

<p>Los ficheros de «log» generados por el proceso de actualización se encuentran en
<url "https://www-master.debian.org/build-logs/webwml/" />
(el script se llama <code>7doc</code> y se ejecuta como parte del
trabajo cron <code>often</code>).</p>

# <p>Tenga en cuenta que este proceso genera de nuevo el directorio
# <code>/doc/manuals/</code>. El contenido del directorio <code>/doc/</code> se genera,
# bien a partir de <a href="/devel/website/desc">«webwml»</a>, bien con otros «scripts»,
# como los que extraen ciertos manuales de sus paquetes.</p>

# <!-- Created: Mon Jul  6 19:58:09 BST 1998 -->
