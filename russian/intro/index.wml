#use wml::debian::template title="Введение в Debian" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e" maintainer="Lev Lamberov"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x"
            style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>Debian &mdash; это сообщество</h2>
      <p>Тысячи добровольцев по всему миру работают сообща над операционной
         системой Debian, отдавая предпочтение свободному и открытому
         программному обеспечению. Познакомьтесь с проектом Debian.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">Люди:</a>
          Кто мы и что мы делаем
        </li>
        <li>
          <a href="philosophy">Наша философия:</a>
          Почему мы это делаем и как мы это делаем
        </li>
        <li>
          <a href="../devel/join/">Принять участие:</a>
          Как стать участником Debian
        </li>
        <li>
          <a href="help">Внести вклад:</a>
          Чем можно помочь Debian
        </li>
        <li>
          <a href="../social_contract">Общественный договор Debian:</a>
          Наши моральные приоритеты
        </li>
        <li>
          <a href="diversity">Приветствуем всех:</a>
          Акт о многообразии
        </li>
        <li>
          <a href="../code_of_conduct">Для участников:</a>
          Кодекс поведения
        </li>
        <li>
          <a href="../partners/">Партнёры:</a>
          Компании и организации, помогающие проекту Debian
        </li>
        <li>
          <a href="../donations">Пожертвования:</a>
          Как поддержать проект Debian
        </li>
        <li>
          <a href="../legal/">Правовые вопросы:</a>
          Лицензии, торговые марки, политика относительно персональных данных,
          политика относительно патентов
        </li>
        <li>
          <a href="../contact">Связаться:</a>
          Как с нами связаться
        </li>
      </ul>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x"
            style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>Debian &mdash; это операционная система</h2>
      <p>Debian является свободной операционной системой, разрабатываемой
         и поддерживаемой проектом Debian. Это свободный дистрибутив Linux
         с тысячами приложений для удовлетворения потребностей наших
         пользователей.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="../distrib">Скачать:</a>
          Где взять Debian
        </li>
        <li>
          <a href="why_debian">Почему Debian:</a>
          Причины выбрать Debian
        </li>
        <li>
          <a href="../support">Поддержка:</a>
          Где найти помощь
        </li>
        <li>
          <a href="../security">Безопасность:</a>
          Последнее обновление<br>
          <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
              @MYLIST = split(/\n/, $MYLIST);
              $MYLIST[0] =~ s#security#../security#;
              print $MYLIST[0]; }:>
        </li>
        <li>
          <a href="../distrib/packages">Программное обеспечение:</a>
          Поиск и просмотр длинного списка пакетов Debian
        </li>
        <li>
          <a href="../doc">Документация:</a>
          Руководство по установке, ЧаВО, HOWTO, Вики-страницы
        </li>
        <li>
          <a href="../bugs">Система отслеживания ошибок:</a>
          Как сообщить об ошибке, документация по системе
        </li>
        <li>
          <a href="https://lists.debian.org/">Списки рассылки:</a>
          Коллекция списков Debian для пользователей, разработчиков
          и&nbsp;т.&nbsp;д.
        </li>
        <li>
          <a href="../blends">Чистые смеси:</a>
          Метапакеты для конкретных нужд
        </li>
        <li>
          <a href="../devel">Уголок разработчика:</a>
          Информация, представляющая интерес в первую очередь для разработчиков
          Debian
        </li>
        <li>
          <a href="../ports">Переносы/Архитектуры:</a>
          Архитектуры процессоров, поддерживаемые Debian
        </li>
        <li>
          <a href="search">Поиск:</a>
          Информация о том, как использовать поисковый движок Debian
        </li>
        <li>
          <a href="cn">Языки:</a>
          Выбор языка для веб-сайта Debian
        </li>
      </ul>
    </div>
  </div>

</div>
