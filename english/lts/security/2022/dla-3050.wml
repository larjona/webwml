<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in the VLC media player,
which could result in the execution of arbitrary code or denial of
service if a malformed media file is opened.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.0.12-0+deb9u1.</p>

<p>We recommend that you upgrade your vlc packages.</p>

<p>For the detailed security status of vlc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/vlc">https://security-tracker.debian.org/tracker/vlc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3050.data"
# $Id: $
