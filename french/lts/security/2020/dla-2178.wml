#use wml::debian::translation-check translation="d833f81c5e32ade4db64d638551cfca7b646e5ee" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Les CVE suivants ont été signalés à l’encontre du paquet source awl.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11728">CVE-2020-11728</a>

<p>Un problème a été découvert dans DAViCal AWL (Andrew's Web Libraries)
jusqu’à la version 0.60. La gestion de session n’utilise pas de clef suffisamment
difficile à deviner pour la session. N’importe qui pouvant apprécier un temps
d’une microseconde (et la variation d’identifiant de session) peut usurper
l’identité de session.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11729">CVE-2020-11729</a>

<p>Un problème a été découvert dans DAViCal AWL (Andrew's Web Libraries)
jusqu’à la version 0.60. Les cookies de session longue, utilisés pour assurer une
permanence de session, ne sont pas créés de manière sécurisée, permettant la
réussite d’une attaque par force brute.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.55-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets awl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2178.data"
# $Id: $
