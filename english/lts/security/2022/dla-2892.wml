<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in the Go programming
language. An attacker could trigger a denial-of-service (DoS) and
information leak.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33196">CVE-2021-33196</a>

    <p>In archive/zip, a crafted file count (in an archive's header) can
    cause a NewReader or OpenReader panic.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36221">CVE-2021-36221</a>

    <p>Go has a race condition that can lead to a net/http/httputil
    ReverseProxy panic upon an ErrAbortHandler abort.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39293">CVE-2021-39293</a>

    <p>Follow-up fix to <a href="https://security-tracker.debian.org/tracker/CVE-2021-33196">CVE-2021-33196</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41771">CVE-2021-41771</a>

    <p>ImportedSymbols in debug/macho (for Open or OpenFat) accesses a
    Memory Location After the End of a Buffer, aka an out-of-bounds
    slice situation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44716">CVE-2021-44716</a>

    <p>net/http allows uncontrolled memory consumption in the header
    canonicalization cache via HTTP/2 requests.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44717">CVE-2021-44717</a>

    <p>Go on UNIX allows write operations to an unintended file or
    unintended network connection as a consequence of erroneous
    closing of file descriptor 0 after file-descriptor exhaustion.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.7.4-2+deb9u4.</p>

<p>We recommend that you upgrade your golang-1.7 packages.</p>

<p>For the detailed security status of golang-1.7 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/golang-1.7">https://security-tracker.debian.org/tracker/golang-1.7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2892.data"
# $Id: $
