#use wml::debian::translation-check translation="71dfc1cac8f8a48fa974b6d10c9a1e5b3f38f31c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un certain nombre de problèmes dans
VideoLAN (c’est-à-dire vlc), un lecteur vidéo et multimédia :</p>


<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25801">CVE-2021-25801</a>

<p>Une vulnérabilité de dépassement de tampon dans le composant __Parse_indx
permettait à des attaquants de provoquer une lecture hors limites à l'aide
d'un fichier .avi contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25802">CVE-2021-25802</a>

<p>Une vulnérabilité de dépassement de tampon dans le composant
AVI_ExtractSubtitle pouvait permettre à des attaquants de provoquer une lecture
hors limites à l'aide d'un fichier .avi contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25803">CVE-2021-25803</a>

<p>Une vulnérabilité de dépassement de tampon dans le composant
vlc_input_attachment_New permettait à des attaquants de provoquer une lecture
hors limites à l'aide d'un fichier .avi contrefait spécialement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25804">CVE-2021-25804</a>

<p>Un déréférencement de pointeur NULL dans « Open » dans avi.c peut aboutir
à une vulnérabilité de déni de service (DoS).</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.0.11-0+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets vlc.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2728.data"
# $Id: $
