#use wml::debian::template title="Notre philosophie : pourquoi nous le faisons et comment nous le faisons" MAINPAGE="true"

#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e" maintainer="Jean-Pierre Giraud"
<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freedom">Notre objectif : créer un système d'exploitation libre</a></li>
    <li><a href="#how">Nos valeurs : comment la communauté fonctionne-t-elle ?</a></li>
  </ul>
</div>

<h2><a id="freedom">Notre objectif : créer un système d'exploitation libre</a></h2>

<p>Le Projet Debian est une association d'individus qui ont pour cause commune
la création d'un système d'exploitation libre, librement disponible pour tous.
Dans ce qui suit, quand nous utilisons le mot « libre », nous ne parlons pas
d'argent, nous nous référons plutôt à la <em>liberté</em> du logiciel.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-unlock-alt fa-2x"></span> <a href="free">Notre définition du logiciel libre</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.gnu.org/philosophy/free-sw">Que dit la Fondation pour le logiciel libre (« Free Software Foundation »)</a></button></p>

<p>Vous vous demandez peut-être pourquoi tant de gens passent des heures de
leur temps libre à réaliser des logiciels, les empaqueter soigneusement et les
entretenir, juste pour les donner gratuitement ? Eh bien, il y a beaucoup de
raisons, en voici quelques unes :</p>

<ul>
  <li>certaines personnes aiment simplement aider les autres, et contribuer à un projet de logiciel libre est une bonne façon de le faire ;</li>
  <li>beaucoup de développeurs écrivent des programmes pour en savoir plus sur les ordinateurs, les différentes architectures et les langages de programmation ;</li>
  <li>certaines personnes contribuent en remerciement de tous les excellents logiciels libres qu'ils ont reçus des autres ;</li>
  <li>beaucoup de gens dans le monde universitaire créent des logiciels libres pour partager les résultats de leurs recherches ;</li>
  <li>des entreprises aident à maintenir des logiciels libres de façon à avoir leur mot à dire sur la façon dont ils sont développés ou pour implémenter de nouvelles fonctionnalités rapidement ;</li>
  <li>et bien sûr, la plupart des développeurs Debian participent parce qu'ils trouvent cela très amusant !</li>
</ul>

<p>Bien que Debian croit au logiciel libre, nous respectons le fait que des
gens ont parfois besoin d'installer des logiciels non libres sur leur machine
– qu'ils le veuillent ou non. Chaque fois que cela est possible, Debian a
décidé d'aider ces utilisateurs. Un nombre croissant de paquets existent dont
l’unique tâche est de permettre d'installer un logiciel non libre dans un
système Debian.</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian est attachée au
logiciel libre et nous avons formalisé cet engagement dans un document : notre
 <a href="$(HOME)/social_contract">Contrat Social</a></p>
</aside>

<h2><a id="how">Nos valeurs : comment la communauté fonctionne-t-elle ?</a></h2>

<p>Le projet Debian compte plus d'un millier de <a
href="people">développeurs et de contributeurs</a> répartis dans <a
href="$(DEVEL)/developers.loc">le monde entier</a>. Un projet de cette
dimension a besoin d'une <a href="organization">structure soigneusement organisée</a>.
Aussi, si vous voulez savoir comment fonctionne le projet Debian et quelles
sont ses règles et ses principes, consultez les documents suivants :</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><a href="$(DEVEL)/constitution">La constitution de Debian</a>: <br>
          ce document décrit la structure organisationnelle et explique comment le projet Debian prend des décisions formelles.</li>
        <li><a href="../social_contract">Le contrat social et les principes du logiciel libre</a> : <br>
          le contrat social et les principes du logiciel libre (DFSG) faisant partie de ce contrat décrivent notre engagement pour le logiciel libre et la communauté du logiciel libre.</li>
        <li><a href="diversity">Les principes de diversité et d'équité :</a> <br>
          le projet Debian accueille et encourage tous les gens à participer quel que soit la manière dont ils s'identifient ou dont les autres les perçoivent.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><a href="../code_of_conduct">Le code de conduite :</a> <br>
          nous avons adopté un code de conduite pour les participants aux listes de diffusion, aux canaux IRC, etc.</li>
        <li><a href="../doc/developers-reference/">Le guide de référence pour Debian :</a> <br>
          ce document fournit une vue d'ensemble des procédures recommandées et des ressources disponibles pour les développeurs et responsables de paquet de Debian.</li>
        <li><a href="../doc/debian-policy/">La charte Debian :</a> <br>
          ce manuel décrit la charte de la distribution Debian, par exemple la structure et le contenu d'une archive Debian, les exigences techniques que chaque paquet doit satisfaire afin d'être inclus dans la distribution, etc.</li>
      </ul>
    </div>
  </div>
</div>        
<p style="text-align:center"><button type="button"><span class="fas fa-code fa-2x"></span> Debian de l'intérieur : <a href="$(DEVEL)/">le coin des développeurs</a></button></p>
