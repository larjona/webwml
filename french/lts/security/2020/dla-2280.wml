#use wml::debian::translation-check translation="20f9d6ae0b0aa4b1a3143c29a82ff95fe537ea9b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans Python, un langage
interactif de haut niveau orienté objet.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20406">CVE-2018-20406</a>

<p>Modules/_pickle.c possédait un dépassement d'entier à cause d'une grande
valeur LONG_BINPUT mal gérée lors d’une requête <q>resize to twice the size</q>.
Ce problème pouvait provoquer un épuisement de mémoire, mais n’est seulement
existant que si le format pickle est utilisé pour sérialiser des dizaines ou des
centaines de gigaoctets de données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20852">CVE-2018-20852</a>

<p>http.cookiejar.DefaultPolicy.domain_return_ok dans Lib/http/cookiejar.py
ne validait pas correctement le domaine : il pouvait être trompé pour envoyer des
cookies existants au mauvais serveur. Un attaquant pouvait utiliser ce défaut
en utilisant un serveur avec un nom d’hôte de même suffixe qu’un autre serveur
autorisé (par exemple, pythonicexample.com pour dérober les cookies
d’example.com). Quand un programme utilisait http.cookiejar.DefaultPolicy et
essayait une connexion HTTP vers un serveur contrôlé par l’attaquant, les cookies
existants pouvait être divulgués à l’attaquant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5010">CVE-2019-5010</a>

<p>Une vulnérabilité exploitable de déni de service existait dans l’analyseur de
certificat X509. Un certificat X509 contrefait pour l'occasion pouvait provoquer
un déréférencement de pointeur NULL, aboutissant à un déni de service. Un
attaquant pouvait initier ou accepter des connexions TLS en utilisant des
certificats contrefaits pour déclencher cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9636">CVE-2019-9636</a>

<p>Traitement impropre de l’encodage Unicode (avec un netloc incorrect) lors
de la normalisation NFKC. L’impact était le suivant : divulgation d'informations
(accréditations, cookies, etc., mis en cache pour un nom d’hôte donné). Les
composants étaient : urllib.parse.urlsplit, urllib.parse.urlparse. Le vecteur
d’attaque était : une URL contrefaite pour l'occasion pouvait être analysée
incorrectement pour localiser les cookies ou les données d’authentification et
les envoyer à un hôte différent que s’ils étaient analysés correctement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a>

<p>Un problème a été découvert dans urllib2. Une injection CRLF était possible
si l’attaquant contrôlait un paramètre d’URL, comme le montre le premier
argument vers urllib.request.urlopen avec \r\n (précisément dans la chaîne de
requête après un caractère « ? ») suivi d’un en-tête HTTP ou d’une commande Redis.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9947">CVE-2019-9947</a>

<p>Un problème a été découvert dans urllib2. Une injection CRLF était possible
si l’attaquant contrôlait un paramètre d’URL, comme le montre le premier
argument vers urllib.request.urlopen avec \r\n (précisément dans le composant de
chemin d’une URL avec un caractère « ? » manquant) suivi d’un en-tête HTTP ou
d’une commande Redis. Cela est similaire au problème de requête de chaîne
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9948">CVE-2019-9948</a>

<p>urllib gérait le schéma local_file:, ce qui facilitait le contournement par des
attaquants distants des mécanismes de protection qui mettaient en liste noire les URI
file:, comme démontré en déclenchant un appel
urllib.urlopen('local_file:///etc/passwd').</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10160">CVE-2019-10160</a>

<p>Une régression de sécurité a été découverte dans Python, qui permet encore
à un attaquant d’exploiter le
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9636">CVE-2019-9636</a>
en traitant incorrectement les parties utilisateur et mot de passe d’une URL.
Quand une application analyse une URL fournie par l’utilisateur pour stocker
des cookies, des accréditations, des authentifications ou d’autres sortes
d’information, il est possible pour un attaquant de fournir des URL contrefaites
pour l'occasion pour que l’application localise les informations relatives
à l’hôte (par exemple, cookies, données d’authentification) et les envoie à un
hôte différent de ce qu'il serait si les URL étaient correctement analysées. Le
résultat de l’attaque peut varier selon l’application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16056">CVE-2019-16056</a>

<p>Le module de courriel analysait mal les adresses de courriel qui contenaient
plusieurs caractères « @ ». Une application utilisant le module de courriel et
mettant en œuvre une sorte de vérification des en-têtes From/To de message
pouvait être trompée pour accepter des adresses qui auraient dû être refusées.
Une attaque pouvant être la même que
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11340">CVE-2019-11340</a>,
mais ce CVE s’applique plus particulièrement à Python.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16935">CVE-2019-16935</a>

<p>Le serveur de documentation XML-RPC possédait un XSS à l’aide du champ
server_title. Cela se produisait dans Lib/xmlrpc/server.py. Si set_server_title
était appelé avec une entrée non approuvée, du JavaScript arbitraire pouvait être
fourni aux clients qui visitait l’URL HTTP de ce serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18348">CVE-2019-18348</a>

<p>Un problème a été découvert dans urllib2. Une injection CRLF était possible
si l’attaquant contrôlait un paramètre d’URL, comme le montre le premier
argument vers urllib.request.urlopen avec \r\n (précisément dans le composant
d’hôte d’une URL) suivi d’un en-tête HTTP. Cela est similaire au problème de
requête de chaîne
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a>
et au problème de chaîne de chemin
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9947">CVE-2019-9947</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8492">CVE-2020-8492</a>

<p>Python permet au serveur HTTP de conduire des attaques de ReDoS (déni de
service à l’aide d’expression rationnelle) contre un client à cause d’une
rétro-inspection catastrophique de urllib.request.AbstractBasicAuthHandler.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14422">CVE-2020-14422</a>

<p>Lib/ipaddress.py calculait improprement les valeurs de hachage dans les
classes IPv4Interface et IPv6Interface. Cela pouvait permettre à un attaquant
distant de provoquer un déni de service si une application était affectée par
les performances d’un dictionnaire contenant des objets IPv4Interface ou
IPv6Interface, et cet attaquant pouvait créer beaucoup d’entrées de dictionnaire.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.5.3-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python3.5.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python3.5, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python3.5">https://security-tracker.debian.org/tracker/python3.5</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2280.data"
# $Id: $
