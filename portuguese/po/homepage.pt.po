msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Thiago Pezzo (tico) <pezzo@protonmail.com>\n"
"Language-Team: \n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "O Sistema Operacional Universal"

#: ../../english/index.def:12
msgid "DC19 Group Photo"
msgstr "Foto coletiva da DC19"

#: ../../english/index.def:15
msgid "DebConf19 Group Photo"
msgstr "Foto coletiva da DebConf19"

#: ../../english/index.def:19
msgid "Mini DebConf Regensburg 2021"
msgstr "Mini DebConf Regensburg 2021"

#: ../../english/index.def:22
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr "Foto coletiva da MiniDebConf em Regensburg 2021"

#: ../../english/index.def:26
msgid "Screenshot Calamares Installer"
msgstr "Captura de tela do instalador Calamares"

#: ../../english/index.def:29
msgid "Screenshot from the Calamares installer"
msgstr "Captura de tela do instalador Calamares"

#: ../../english/index.def:33 ../../english/index.def:36
msgid "Debian is like a Swiss Army Knife"
msgstr "Debian é como um canivete suíço"

#: ../../english/index.def:40
msgid "People have fun with Debian"
msgstr "Pessoas se divertem com o Debian"

#: ../../english/index.def:43
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "Pessoal do Debian na Debconf18 em Hsinchu, se divertindo de verdade"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "Bits do Debian"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "Blog"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "Micronotícias"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "Micronotícias do Debian"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "Planeta"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "O Planeta Debian"
