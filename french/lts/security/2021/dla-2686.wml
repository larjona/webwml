#use wml::debian::translation-check translation="cea2e92155b7909ecf57574f339e401f9ed25744" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans python-urllib3, un client
HTTP pour Python.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20060">CVE-2018-20060</a>

<p>Urllib3 ne retire pas l’en-tête d’autorisation HTTP lors du suivi d’une
redirection d’origine croisée (c’est-à-dire une redirection qui diffère dans
l’hôte, le port ou le schéma). Cela peut permettre l'exposition à des hôtes
non souhaités des accréditations dans l’en-tête d’authentification ou leur
transmission en clair.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11236">CVE-2019-11236</a>

<p>Une injection CRLF est possible si l’attaquant contrôle le paramètre de la
requête.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11324">CVE-2019-11324</a>

<p>Urllib3 gère incorrectement certain cas où l’ensemble voulu de certificats CA
est différent du magasin de certificats CA du système d’exploitation, ce qui
aboutit à des connexions SSL réussies dans des situations où un échec de
vérification est le résultat correct. Cela concerne les arguments ssl_context,
ca_certs ou ca_certs_dir.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26137">CVE-2020-26137</a>

<p>Urllib3 permet une injection CRLF si l’attaquant contrôle la méthode de
requête HTTP, comme cela est démontré en insérant des caractères de contrôle CR
et LF dans le premier argument de putrequest().</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.19.1-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-urllib3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-urllib3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-urllib3">\
https://security-tracker.debian.org/tracker/python-urllib3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2686.data"
# $Id: $
