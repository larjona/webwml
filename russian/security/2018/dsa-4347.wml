#use wml::debian::translation-check translation="165625c3a669960bb5e1c766db812564b5fd665e" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В реализации языка программирования Perl были обнаружены многочисленные
уязвимости. Проект Common Vulnerabilities and Exposures
определяет следующие проблемы:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18311">CVE-2018-18311</a>

    <p>Джайакришна Менон и Кристоф Хайзер обнаружили переполнение целых
    чисел в Perl_my_setenv, приводящее к переполнению буфера при
    обработке входных данных злоумышленника.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18312">CVE-2018-18312</a>

    <p>Эичи Тсуката обнаружил, что специально сформированное регулярное выражение может
    приводить к записи за пределами выделенного буфера памяти во время компиляции,
    что потенциально позволяет выполнять произвольный код.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18313">CVE-2018-18313</a>

    <p>Эичи Тсуката обнаружил, что специально сформированное регулярное выражение может
    приводить к записи за пределами выделенного буфера памяти во время компиляции,
    что приводит к утечке информации.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18314">CVE-2018-18314</a>

    <p>Якуб Вилк обнаружил, что специально сформированное регулярное выражение
    может приводить к переполнению буфера.</p></li>

</ul>

<p>В стабильном выпуске (stretch) эти проблемы были исправлены в
версии 5.24.1-3+deb9u5.</p>

<p>Рекомендуется обновить пакеты perl.</p>

<p>С подробным статусом поддержки безопасности perl можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/perl">\
https://security-tracker.debian.org/tracker/perl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4347.data"
