<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability was discovered in HAProxy, a fast and reliable
load balancing reverse proxy, which may result in denial of service, or
bypass of access controls and routing rules via specially crafted requests.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.8.19-1+deb10u4.</p>

<p>We recommend that you upgrade your haproxy packages.</p>

<p>For the detailed security status of haproxy please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/haproxy">https://security-tracker.debian.org/tracker/haproxy</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3318.data"
# $Id: $
