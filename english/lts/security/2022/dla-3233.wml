<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update fixes a denial of service vulnerability in leptonlib. It can be
made to crash with an arithmetic exception on specially crafted JPEG files.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.76.0-1+deb10u2.</p>

<p>We recommend that you upgrade your leptonlib packages.</p>

<p>For the detailed security status of leptonlib please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/leptonlib">https://security-tracker.debian.org/tracker/leptonlib</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3233.data"
# $Id: $
