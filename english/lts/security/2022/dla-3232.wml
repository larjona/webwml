<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in virglrenderer, a
virtual GPU for KVM virtualization.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18388">CVE-2019-18388</a>

    <p>A NULL pointer dereference in vrend_renderer.c in virglrenderer through
    0.8.0 allows guest OS users to cause a denial of service via malformed
    commands.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18389">CVE-2019-18389</a>

    <p>A heap-based buffer overflow in the vrend_renderer_transfer_write_iov
    function in vrend_renderer.c in virglrenderer through 0.8.0 allows
    guest OS users to cause a denial of service, or QEMU guest-to-host
    escape and code execution, via VIRGL_CCMD_RESOURCE_INLINE_WRITE
    commands.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18390">CVE-2019-18390</a>

    <p>An out-of-bounds read in the vrend_blit_need_swizzle function in
    vrend_renderer.c in virglrenderer through 0.8.0 allows guest OS
    users to cause a denial of service via VIRGL_CCMD_BLIT commands.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18391">CVE-2019-18391</a>

    <p>A heap-based buffer overflow in the vrend_renderer_transfer_write_iov
    function in vrend_renderer.c in virglrenderer through 0.8.0 allows
    guest OS users to cause a denial of service via
    VIRGL_CCMD_RESOURCE_INLINE_WRITE commands.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8002">CVE-2020-8002</a>

    <p>A NULL pointer dereference in vrend_renderer.c in virglrenderer through
    0.8.1 allows attackers to cause a denial of service via commands that attempt
    to launch a grid without previously providing a Compute Shader (CS).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8003">CVE-2020-8003</a>

    <p>A double-free vulnerability in vrend_renderer.c in virglrenderer through
    0.8.1 allows attackers to cause a denial of service by triggering texture
    allocation failure, because vrend_renderer_resource_allocated_texture is not an
    appropriate place for a free.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0135">CVE-2022-0135</a>

    <p>An out-of-bounds write issue was found in the VirGL virtual OpenGL renderer
    (virglrenderer). This flaw allows a malicious guest to create a specially
    crafted virgil resource and then issue a VIRTGPU_EXECBUFFER ioctl, leading to a
    denial of service or possible code execution.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.7.0-2+deb10u1.</p>

<p>We recommend that you upgrade your virglrenderer packages.</p>

<p>For the detailed security status of virglrenderer please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/virglrenderer">https://security-tracker.debian.org/tracker/virglrenderer</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3232.data"
# $Id: $
