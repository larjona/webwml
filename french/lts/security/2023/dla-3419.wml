#use wml::debian::translation-check translation="aa896f9b55e0c9a2df5639e7bea053d17976e482" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
WebKitGTK.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0108">CVE-2022-0108</a>

<p>Luan Herrera a découvert qu’un document HTML pouvait produire le rendu
d’<code>iframe</code>s avec des informations sensibles de l’utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32885">CVE-2022-32885</a>

<p>P1umer et Q1IQ ont découvert que le traitement de contenu web malveillant
pouvait conduire à l’exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27932">CVE-2023-27932</a>

<p>Un chercheur anonyme a découvert que le traitement de contenu web malveillant
pouvait contourner la politique de même origine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27954">CVE-2023-27954</a>

<p>Un chercheur anonyme a découvert qu’un site web pouvait suivre des
informations sensibles de l’utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28205">CVE-2023-28205</a>

<p>Clément Lecigne et Donncha O Cearbhaill ont découvert que le traitement de
contenu web malveillant pouvait conduire à l’exécution de code arbitraire.
Apple est conscient que ce problème a pu être largement exploité.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.38.6-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3419.data"
# $Id: $
