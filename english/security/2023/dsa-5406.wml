<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Max Chernoff discovered that improperly secured shell-escape in LuaTeX
may result in arbitrary shell command execution, even with shell escape
disabled, if specially crafted tex files are processed.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2020.20200327.54578-7+deb11u1.</p>

<p>We recommend that you upgrade your texlive-bin packages.</p>

<p>For the detailed security status of texlive-bin please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/texlive-bin">https://security-tracker.debian.org/tracker/texlive-bin</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5406.data"
# $Id: $
