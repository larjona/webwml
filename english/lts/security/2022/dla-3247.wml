<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential remote denial of service
vulnerability in node-trim-newlines, a Javascript module to strip newlines from
the start and/or end of a string.</p>

<p>This regular expression Denial of Service (ReDoS) attack exploited the fact
that most Regular Expression implementations can reach extreme situations that
cause them to work very slowly in a way that is exponentially related to the
input size. An attacker can then cause a program using node-trim-newlines (and
thus the offending regex) to enter one of these extreme situations and then
hang for a very long time.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33623">CVE-2021-33623</a>

    <p>The trim-newlines package before 3.0.1 and 4.x before 4.0.1 for Node.js
    has an issue related to regular expression denial-of-service (ReDoS) for
    the .end() method.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
1.0.0-1+deb10u1.</p>

<p>We recommend that you upgrade your node-trim-newlines packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3247.data"
# $Id: $
