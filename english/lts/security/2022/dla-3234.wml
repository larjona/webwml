<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that those using java.sql.Statement or java.sql.PreparedStatement
in hsqldb, a Java SQL database, to process untrusted input may be vulnerable to
a remote code execution attack. By default it is allowed to call any static
method of any Java class in the classpath resulting in code execution. The
issue can be prevented by updating to 2.4.1-2+deb10u1 or by setting the
system property <q>hsqldb.method_class_names</q> to classes which are allowed to
be called. For example, System.setProperty("hsqldb.method_class_names","abc")
or Java argument -Dhsqldb.method_class_names="abc" can be used. From
version 2.4.1-2+deb10u1 all classes by default are not accessible except
those in java.lang.Math and need to be manually enabled.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.4.1-2+deb10u1.</p>

<p>We recommend that you upgrade your hsqldb packages.</p>

<p>For the detailed security status of hsqldb please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/hsqldb">https://security-tracker.debian.org/tracker/hsqldb</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3234.data"
# $Id: $
