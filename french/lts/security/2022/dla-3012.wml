#use wml::debian::translation-check translation="ad5929e547301fc29cda691873a9b3aeea56579b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Felix Wilhelm a découvert que libxml2, la bibliothèque XML de GNOME, ne
vérifiait pas correctement les dépassements d'entiers ou l'utilisation de
types erronés pour la taille des tampons. Cela pouvait avoir pour
conséquences des écritures hors limites ou d'autres erreurs de mémoire
lors du traitement de grands tampons de plusieurs gigaoctets.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.9.4+dfsg1-2.2+deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxml2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxml2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libxml2">\
https://security-tracker.debian.org/tracker/libxml2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3012.data"
# $Id: $
