#use wml::debian::translation-check translation="e2bf46b07c9c5e10d7fa4cf7daaafc92ebfc04c6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités, telles que des traversées de répertoires, ReDoS et
autres, ont été trouvées dans ruby-rack, une interface de serveur web modulaire en
Ruby.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.0.6-3+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-rack.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-rack,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-rack">\
https://security-tracker.debian.org/tracker/ruby-rack</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3298.data"
# $Id: $
