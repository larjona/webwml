<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that sysstat, a system performance tools for Linux,
incompletely fixed <a href="https://security-tracker.debian.org/tracker/CVE-2022-39377">CVE-2022-39377</a> (as published in DLA-3188-1), which
could lead to crashes and possibly remote code execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33204">CVE-2023-33204</a>

    <p>sysstat allows a multiplication integer overflow in check_overflow
    in common.c. NOTE: this issue exists because of an incomplete fix
    for <a href="https://security-tracker.debian.org/tracker/CVE-2022-39377">CVE-2022-39377</a>.</p>

<p>For reference, the initial vulnerability was:</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39377">CVE-2022-39377</a>

    <p>On 32 bit systems, allocate_structures contains a size_t overflow
    in sa_common.c. The allocate_structures function insufficiently
    checks bounds before arithmetic multiplication, allowing for an
    overflow in the size allocated for the buffer representing system
    activities. This issue may lead to Remote Code Execution (RCE).</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
12.0.3-2+deb10u2.</p>

<p>We recommend that you upgrade your sysstat packages.</p>

<p>For the detailed security status of sysstat please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sysstat">https://security-tracker.debian.org/tracker/sysstat</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3434.data"
# $Id: $
