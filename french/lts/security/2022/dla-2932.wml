#use wml::debian::translation-check translation="1ffe8d2358a67c58ec372e0d15d53b0b224a2f99" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans tiff, une bibliothèque et
des outils pour manipuler et convertir des fichiers au format TIFF (Tag
Image File Format.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22844">CVE-2022-22844</a>

<p>Une lecture hors limites dans _TIFFmemcpy dans certaines situations
impliquant une marque personnalisée et 0x0200 comme second mot du champ DE.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0562">CVE-2022-0562</a>

<p>Un pointeur de source NULL passé comme argument à la fonction memcpy()
dans TIFFReadDirectory(). Cela pouvait avoir pour conséquence un déni de
service au moyen de fichiers TIFF contrefaits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0561">CVE-2022-0561</a>

 <p>Un pointeur de source NULL passé comme argument à la fonction memcpy()
dans TIFFFetchStripThing(). Cela pouvait avoir pour conséquence un déni de
service au moyen de fichiers TIFF contrefaits.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4.0.8-2+deb9u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tiff, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tiff">\
https://security-tracker.debian.org/tracker/tiff</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2932.data"
# $Id: $
