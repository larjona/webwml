<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the chromium web browser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6423">CVE-2020-6423</a>

    <p>A use-after-free issue was found in the audio implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6430">CVE-2020-6430</a>

    <p>Avihay Cohen discovered a type confusion issue in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6431">CVE-2020-6431</a>

    <p>Luan Herrera discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6432">CVE-2020-6432</a>

    <p>Luan Herrera discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6433">CVE-2020-6433</a>

    <p>Luan Herrera discovered a policy enforcement error in extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6434">CVE-2020-6434</a>

    <p>HyungSeok Han discovered a use-after-free issue in the developer tools.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6435">CVE-2020-6435</a>

    <p>Sergei Glazunov discovered a policy enforcement error in extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6436">CVE-2020-6436</a>

    <p>Igor Bukanov discovered a use-after-free issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6437">CVE-2020-6437</a>

    <p>Jann Horn discovered an implementation error in WebView.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6438">CVE-2020-6438</a>

    <p>Ng Yik Phang discovered a policy enforcement error in extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6439">CVE-2020-6439</a>

    <p>remkoboonstra discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6440">CVE-2020-6440</a>

    <p>David Erceg discovered an implementation error in extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6441">CVE-2020-6441</a>

    <p>David Erceg discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6442">CVE-2020-6442</a>

    <p>B@rMey discovered an implementation error in the page cache.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6443">CVE-2020-6443</a>

    <p>@lovasoa discovered an implementation error in the developer tools.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6444">CVE-2020-6444</a>

    <p>mlfbrown discovered an uninitialized variable in the WebRTC
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6445">CVE-2020-6445</a>

    <p>Jun Kokatsu discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6446">CVE-2020-6446</a>

    <p>Jun Kokatsu discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6447">CVE-2020-6447</a>

    <p>David Erceg discovered an implementation error in the developer tools.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6448">CVE-2020-6448</a>

    <p>Guang Gong discovered a use-after-free issue in the v8 javascript library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6454">CVE-2020-6454</a>

    <p>Leecraso and Guang Gong discovered a use-after-free issue in extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6455">CVE-2020-6455</a>

    <p>Nan Wang and Guang Gong discovered an out-of-bounds read issue in the
    WebSQL implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6456">CVE-2020-6456</a>

    <p>Michał Bentkowski discovered insufficient validation of untrusted input.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6457">CVE-2020-6457</a>

    <p>Leecraso and Guang Gong discovered a use-after-free issue in the speech
    recognizer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6458">CVE-2020-6458</a>

    <p>Aleksandar Nikolic discoved an out-of-bounds read and write issue in the
    pdfium library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6459">CVE-2020-6459</a>

    <p>Zhe Jin discovered a use-after-free issue in the payments implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6460">CVE-2020-6460</a>

    <p>It was discovered that URL formatting was insufficiently validated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6461">CVE-2020-6461</a>

    <p>Zhe Jin discovered a use-after-free issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6462">CVE-2020-6462</a>

    <p>Zhe Jin discovered a use-after-free issue in task scheduling.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6463">CVE-2020-6463</a>

    <p>Pawel Wylecial discovered a use-after-free issue in the ANGLE library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6464">CVE-2020-6464</a>

    <p>Looben Yang discovered a type confusion issue in Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6465">CVE-2020-6465</a>

    <p>Woojin Oh discovered a use-after-free issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6466">CVE-2020-6466</a>

    <p>Zhe Jin discovered a use-after-free issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6467">CVE-2020-6467</a>

    <p>ZhanJia Song discovered a use-after-free issue in the WebRTC
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6468">CVE-2020-6468</a>

    <p>Chris Salls and Jake Corina discovered a type confusion issue in the v8
    javascript library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6469">CVE-2020-6469</a>

    <p>David Erceg discovered a policy enforcement error in the developer tools.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6470">CVE-2020-6470</a>

    <p>Michał Bentkowski discovered insufficient validation of untrusted input.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6471">CVE-2020-6471</a>

    <p>David Erceg discovered a policy enforcement error in the developer tools.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6472">CVE-2020-6472</a>

    <p>David Erceg discovered a policy enforcement error in the developer tools.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6473">CVE-2020-6473</a>

    <p>Soroush Karami and Panagiotis Ilia discovered a policy enforcement error
    in Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6474">CVE-2020-6474</a>

    <p>Zhe Jin discovered a use-after-free issue in Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6475">CVE-2020-6475</a>

    <p>Khalil Zhani discovered a user interface error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6476">CVE-2020-6476</a>

    <p>Alexandre Le Borgne discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6478">CVE-2020-6478</a>

    <p>Khalil Zhani discovered an implementation error in full screen mode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6479">CVE-2020-6479</a>

    <p>Zhong Zhaochen discovered an implementation error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6480">CVE-2020-6480</a>

    <p>Marvin Witt discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6481">CVE-2020-6481</a>

    <p>Rayyan Bijoora discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6482">CVE-2020-6482</a>

    <p>Abdulrahman Alqabandi discovered a policy enforcement error in the
    developer tools.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6483">CVE-2020-6483</a>

    <p>Jun Kokatsu discovered a policy enforcement error in payments.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6484">CVE-2020-6484</a>

    <p>Artem Zinenko discovered insufficient validation of user data in the
    ChromeDriver implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6485">CVE-2020-6485</a>

    <p>Sergei Glazunov discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6486">CVE-2020-6486</a>

    <p>David Erceg discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6487">CVE-2020-6487</a>

    <p>Jun Kokatsu discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6488">CVE-2020-6488</a>

    <p>David Erceg discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6489">CVE-2020-6489</a>

    <p>@lovasoa discovered an implementation error in the developer tools.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6490">CVE-2020-6490</a>

    <p>Insufficient validation of untrusted data was discovered.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6491">CVE-2020-6491</a>

    <p>Sultan Haikal discovered a user interface error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6493">CVE-2020-6493</a>

    <p>A use-after-free issue was discovered in the WebAuthentication
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6494">CVE-2020-6494</a>

    <p>Juho Nurimen discovered a user interface error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6495">CVE-2020-6495</a>

    <p>David Erceg discovered a policy enforcement error in the developer tools.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6496">CVE-2020-6496</a>

    <p>Khalil Zhani discovered a use-after-free issue in payments.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6497">CVE-2020-6497</a>

    <p>Rayyan Bijoora discovered a policy enforcement issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6498">CVE-2020-6498</a>

    <p>Rayyan Bijoora discovered a user interface error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6505">CVE-2020-6505</a>

    <p>Khalil Zhani discovered a use-after-free issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6506">CVE-2020-6506</a>

    <p>Alesandro Ortiz discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6507">CVE-2020-6507</a>

    <p>Sergei Glazunov discovered an out-of-bounds write issue in the v8
    javascript library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6509">CVE-2020-6509</a>

    <p>A use-after-free issue was discovered in extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6831">CVE-2020-6831</a>

    <p>Natalie Silvanovich discovered a buffer overflow issue in the SCTP
    library.</p></li>

</ul>

<p>For the oldstable distribution (stretch), security support for chromium
has been discontinued.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 83.0.4103.116-1~deb10u1.</p>

<p>We recommend that you upgrade your chromium packages.</p>

<p>For the detailed security status of chromium please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4714.data"
# $Id: $
