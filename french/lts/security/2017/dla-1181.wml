#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l’hyperviseur Xen.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15588">CVE-2017-15588</a>

<p>Jann Horn a découvert une situation de compétition pouvant provoquer qu’une
entrée TLB périmée aboutisse à une élévation des privilèges, un déni de service
ou une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15589">CVE-2017-15589</a>

<p>Roger Pau Monné a découvert une fuite de pile d’hyperviseur dans le code
d’interception d’E/S x86, aboutissant à une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15592">CVE-2017-15592</a>

<p>Andrew Cooper a découvert que le traitement incorrect de mappages
« self-linear shadow » pour convertir des clients pourrait aboutir à un déni de
service ou une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15593">CVE-2017-15593</a>

<p>Jan Beulich a découvert que le compte de références de type de page est mal
géré. Cela pourrait aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15595">CVE-2017-15595</a>

<p>Jann Horn a découvert qu’un empilement de « page-table » contrefait pourrait
aboutir à un déni de service, une élévation des privilèges ou une fuite
d'informations.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.1.6.lts1-10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xen.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1181.data"
# $Id: $
