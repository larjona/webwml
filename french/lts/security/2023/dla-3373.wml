#use wml::debian::translation-check translation="027638d67f313a548eeb4cf69c6fd01ab9a84a3e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans la bibliothèque Json-smart.
Json-smart est une bibliothèque, centrée sur la performance, de traitement JSON
écrite en Java.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31684">CVE-2021-31684</a>

<p>Une vulnérabilité a été découverte dans la fonction indexOf de
JSONParserByteArray dans JSON Smart, versions 1.3 et 2.4, qui causait un déni de
service (DOS) à l’aide, par exemple, d’une requête web contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1370">CVE-2023-1370</a>

<p>Un débordement de pile a été découvert, provoqué par une récursion excessive.
Lorsque qu’un caractère <q>[</q> ou <q>{</q> se présente dans l’entrée JSON, le
code analyse respectivement un tableau ou un objet. Il a été découvert que le
code n’avait aucune limite dans l’imbrication de tels tableaux ou objets.
Puisque leur analyse était réalisée de manière récursive, leur imbrication en
trop grand nombre pouvait provoquer un épuisement (débordement) de pile et
planter le logiciel.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.2-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets json-smart.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de json-smart,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/json-smart">\
https://security-tracker.debian.org/tracker/json-smart</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3373.data"
# $Id: $
