<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in the web-based database tool
Adminer whereby an attacker could have performed an Arbitrary File Read on the
remote server by requesting Adminer connect to a crafted remote MySQL
database.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43008">CVE-2021-43008</a>

    <p>CVE-2021-43008: Improper Access Control in Adminer versions 1.12.0 to
    4.6.2 (fixed in version 4.6.3) allows an attacker to achieve Arbitrary File
    Read on the remote server by requesting the Adminer to connect to a remote
    MySQL database.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
4.2.5-3+deb9u3.</p>

<p>We recommend that you upgrade your adminer packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3002.data"
# $Id: $
