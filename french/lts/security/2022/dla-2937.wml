#use wml::debian::translation-check translation="85521ea45acc2af3419279d9bcca365a4bbb3693" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Trois problèmes ont été découverts dans gif2apng, un outil pour
convertir des images GIF animées au format APNG.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45909">CVE-2021-45909</a>

<p>Une vulnérabilité de dépassement de tampon de tas dans la fonction
DecodeLZW. Elle permet à un attaquant d'écrire une grande quantité de
données arbitraires en dehors des limites d'un tampon.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45910">CVE-2021-45910</a>:

<p>Un dépassement de tampon de tas dans la fonction principale. Elle permet
à un attaquant d'écrire des données en dehors du tampon alloué.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45911">CVE-2021-45911</a>:

<p>Un dépassement de tampon de tas dans le traitement des délais dans la
fonction principale.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.9+srconly-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gif2apng.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gif2apng, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gif2apng">\
https://security-tracker.debian.org/tracker/gif2apng</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2937.data"
# $Id: $
