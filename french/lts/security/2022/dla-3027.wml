#use wml::debian::translation-check translation="386c97a0cc296981233efe3bf631fe91c9d53745" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>L'envoi précédent de neutron pour Debian 9 <q>Stretch</q> (c'est-à-dire
la version 2:9.1.1-3+deb9u2) était incomplète et n'appliquait pas
réellement le correctif pour le CVE-2021-40085.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40085">CVE-2021-40085</a>

<p>Un problème a été découvert dans les Neutron d'OpenStack avant la
version 16.4.1, 17.x avant la version 17.2.1 et 18.x avant la
version 18.1.1. Des attaquants authentifiés peuvent reconfigurer dnsmasq à
l'aide d'une valeur extra_dhcp_opts contrefaite.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème est maintenant corrigé dans la
version 2:9.1.1-3+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets neutron.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3027.data"
# $Id: $
