#use wml::debian::template title="Debian voor PowerPC -- Installatie " NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/powerpc/inst/menu.inc"
#use wml::debian::translation-check translation="70cf45edbaeb4b8fc8f99d683f2f5c5c4435be92"

<h2> Installatie van Debian GNU/Linux op PowerPC-machines </h2>
<p>
 Raadpleeg de volgende pagina's voor specifieke informatie over
 het installeren van Debian/PowerPC op uw systeem:
</p>
<ul>
 <li> <a href="apus">Amiga PowerUP-systeem</a> </li>
 <li> <a href="chrp">CHRP</a> </li>
 <li> <a href="prep">PReP</a> </li>
 <li> <a href="pmac">PowerMac</a> </li>
</ul>
     <p>
Er worden vier belangrijke <em>powerpc</em>-varianten ondersteund:
PMac (Power-Macintosh), Apus, CHRP en PReP.  Aan aanpassingen in functie
van andere <em>powerpc</em>-architecturen, zoals de Be-Box- en de
MBX-architectuur wordt gewerkt, maar deze worden nu nog niet ondersteund
door Debian. Mogelijk kont er in de toekomst een 64-bits versie.

<p>
In Debian zijn er ook vier powerpc kernel-varianten. Deze zijn gebaseerd
op het specifieke CPU-type dat wordt gebruikt (en mag niet verward worden
met de architectuurvarianten waarvan hierboven sprake was):</p>

<div><dl>

<dt>powerpc</dt>

<dd><p>
Voor de meeste systemen wordt deze kernel-variant gebruikt, die de PowerPC
601, 603, 604, 740, 750, en 7400 processoren ondersteunt. Alle Apple Power
Macintosh systemen tot en met de G4 hebben processoren die door deze kernel
ondersteund worden.</p></dd>

<dt>power3</dt>

<dd><p>
De POWER3 processor wordt gebruikt in oudere IBM 64-bits serversystemen:
bekende modellen zijn het IntelliStation POWER Model 265, de pSeries 610 en
640 en de RS/6000 7044-170, 7043-260, en 7044-270.</p></dd>

<dt>power4</dt>

<dd><p>
De POWER4 processor wordt gebruikt in recentere IBM 64-bits serversystemen:
bekende modellen zijn de pSeries 615, 630, 650, 655, 670 en 690.
De Apple G5 is ook gebaseerd op de POWER4 architectuur en maakt gebruik
van deze kernel-variant.</p></dd>

<dt>apus</dt>

<dd><p>
Deze kernel-variant ondersteunt het Amiga Power-UP-systeem.
</p></dd>

</dl></div>

     <p>
Apple (en ook enkele andere fabrikanten - Power Computing bijvoorbeeld)
maakt een reeks Macintosh computers, gebaseerd op de PowerPC
processor. In functie van de architectuurondersteuning
worden ze opgedeeld in Nubus, OldWorld PCI en NewWorld.
     <p>
Nubus systemen worden momenteel niet ondersteund door debian/powerpc. De
monolitische Linux/PPC kernel-architectuur biedt geen ondersteuning voor
deze machines; in plaats daarvan moet men de MkLinux Mach microkernel
gebruiken, welke Debian momenteel nog niet ondersteunt. Het betreft de
6100/7100/8100 reeks van Power Macintoshes.
     <p>
Tot de OldWorld systemen behoren de meeste Power Macintoshes met een
diskettestation en een PCI bus. De meeste 603, 603e, 604 en op 604e
gebaseerde Power Macintoshes zijn OldWorld machines. De beigekleurige G3
systemen zijn ook OldWorld.
     <p>
De zogenaamde NewWorld PowerMacs zijn alle PowerMacs in doorschijnende
gekleurde plastic behuizing, alle iMacs, iBooks, G4 en G5 systemen. De
NewWorld PowerMacs staan ook bekend voor het gebruik van het `ROM in RAM'
systeem voor het Mac OS en werden vanaf midden 1998 gefabriceerd.

<p>Hier volgt een lijst van powerpc machines waarop Debian zou moeten werken.</p>

<table class="reltable">
<colgroup span="2">
<tr>
  <th><strong>Model Naam/Nummer</strong></th>
  <th><strong>Architectuur</strong></th>
</tr>
<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>Apple</strong></td>
  <td></td>
</tr>

<tr class="even"><td>iMac Bondi Blauw, 5 varianten, sleufsysteem</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>iMac zomer 2000, voorjaar 2001</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>iMac G5</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>iBook, iBook SE, iBook Dual USB</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>iBook2</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>iBook G4</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>Power Macintosh blauw en wit (B&amp;W) G3</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>Power Macintosh G4 PCI, AGP, kubus</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>Power Macintosh G4 Gigabit Ethernet</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>Power Macintosh G4 Digital Audio, Quicksilver</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>Power Macintosh G5</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>PowerBook G3 FireWire Pismo (2000)</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>PowerBook G3 Lombard (1999)</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>PowerBook G4 Titanium</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>PowerBook G4 Aluminium</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>Mac mini</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td>Xserve G5</td>  <td><a href="pmac">powermac-NewWorld</a></td></tr>
<tr class="even"><td colspan="2"></td></tr>

<tr class="even"><td>Performa 4400, 54xx, 5500</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Performa 6300, 6360, 6400, 6500</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Power Macintosh 4400, 5400</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Power Macintosh 7200, 7300, 7500, 7600</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Power Macintosh 8200, 8500, 8600</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Power Macintosh 9500, 9600</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Power Macintosh (Beige) G3 Minitower</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Power Macintosh (Beige) Desktop, All-in-One</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>PowerBook 2400, 3400, 3500</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>PowerBook G3 Wallstreet (1998)</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Twintigste verjaardags-Macintosh</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Werkgroepserver 7250, 8550, 9650, G3</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>

<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>Power Computing</strong></td>
  <td></td>
</tr>

<tr class="even"><td>PowerBase, PowerTower / Pro, PowerWave</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>PowerCenter / Pro, PowerCurve</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>

<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>UMAX</strong></td>
  <td></td>
</tr>

<tr class="even"><td>C500, C600, J700, S900</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>

<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>APS</strong></td>
  <td></td>
</tr>

<tr class="even"><td>APS Tech M*Power 604e/2000</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>

<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>Motorola</strong></td>
  <td></td>
</tr>

<tr class="even"><td>Starmax 3000, 4000, 5000, 5500</td>  <td><a href="pmac">powermac-OldWorld</a></td></tr>
<tr class="even"><td>Firepower, PowerStack Series E, PowerStack II</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>MPC 7xx, 8xx</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>MTX, MTX+</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>MVME2300(SC)/24xx/26xx/27xx/36xx/46xx</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>MCP(N)750</td>  <td><a href="prep">PReP</a></td></tr>

<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>IBM RS/6000</strong></td>
  <td></td>
</tr>

<tr class="even"><td>40P, 43P</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>Power 830/850/860 (6070, 6050)</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>6015, 6030, 7025, 7043</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>p640</td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>B50, 43-P150, 44P</td>  <td><a href="chrp">CHRP</a></td></tr>

<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>Genesi</strong></td>
  <td></td>
</tr>

<tr class="even"><td>Pegasos I, Pegasos II</td>  <td><a href="chrp">CHRP</a></td></tr>

<tr class="even"><td colspan="2"></td></tr>
<tr class="odd">
  <td><strong>Amiga Power-UP Systemen (APUS)</strong></td>
  <td></td>
</tr>

<tr class="even"><td>A1200, A3000, A4000</td>  <td><a href="apus">APUS</a></td></tr>
</table>
<p>
Hier volgt een lijst machines waarvan we niet weten of ze met Debian
werken. Mogelijk werken ze wel, en we horen het graag van u mocht u er een
getest hebben met enig succes.
</p>
<table class="reltable">
<colgroup span="2">
<tr>
  <th><strong>Model Naam/Nummer</strong></th>
  <th><strong>Architectuur</strong></th>
</tr>
<tr class="even"><td>IBM Longtrail II, eerste 'vrij' of 'open' PowerPC-bord</td>   <td><a href="chrp">CHRP</a></td></tr>
<tr class="even"><td>7248-100,7248-120,7248-132 </td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>Notebook Thinkpad 820: 7247-821/822/823 </td>  <td><a href="prep">PReP</a></td></tr>
<tr class="even"><td>Notebook Thinkpad 850: 7247-851/860 </td>  <td><a href="prep">PReP</a></td></tr>
</table>











