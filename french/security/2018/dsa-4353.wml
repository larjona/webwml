#use wml::debian::translation-check translation="c4f5a9b7b36060e89ec006b8bca2732d18d8bf8a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans PHP, un langage
de script généraliste à source libre couramment utilisé : le module EXIF
était vulnérable à un déni de service ou à une divulgation d'informations
lors de l'analyse d'images mal formées, le module Apache permettait une
attaque par script intersite au moyen du corps d'une requête
« Transfer-Encoding: chunked » et l'extension IMAP réalisait une validation
d'entrée insuffisante qui peut avoir pour conséquence l'exécution de
commandes de shell arbitraires dans la fonction imap_open() et un déni de
service dans la fonction imap_mail().</p>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 7.0.33-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php7.0.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php7.0, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php7.0">\
https://security-tracker.debian.org/tracker/php7.0</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4353.data"
# $Id: $
