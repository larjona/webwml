<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>HTML::StripScripts, a module for removing scripts from HTML, allows
_hss_attval_style ReDoS because of catastrophic backtracking for HTML
content with certain style attributes.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.06-1+deb10u1.</p>

<p>We recommend that you upgrade your libhtml-stripscripts-perl packages.</p>

<p>For the detailed security status of libhtml-stripscripts-perl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libhtml-stripscripts-perl">https://security-tracker.debian.org/tracker/libhtml-stripscripts-perl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3296.data"
# $Id: $
