#use wml::debian::translation-check translation="b61fdbabc659004fe9c2d4bc5287a389d74d8d1a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3759">CVE-2021-3759</a>

<p>Il a été découvert que le contrôleur de mémoire cgroup ne tenait pas compte
de la mémoire du noyau allouée pour les objets IPC. Un utilisateur local
pouvait utiliser cela pour un déni de service (épuisement de mémoire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3169">CVE-2022-3169</a>

<p>Il a été découvert que le pilote NVMe d’hôte n’empêchait pas une
réinitialisation concurrente ou d’un sous-système. Un utilisateur local avec
accès à un périphérique NVMe pourrait utiliser cela pour provoquer un déni de
service (déconnexion du périphérique ou plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3435">CVE-2022-3435</a>

<p>Gwangun Jung a signalé un défaut dans le sous-système de retransmission IPv4
qui pouvait aboutir à une lecture hors limites. Un utilisateur local ayant la
capacité CAP_NET_ADMIN dans n’importe quel espace de nommage utilisateur
pourrait éventuellement exploiter cela pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3521">CVE-2022-3521</a>

<p>L’outil syzbot a trouvé une situation de compétition dans le sous-système KCM
qui pourrait conduire à plantage.</p>

<p>Ce sous-système n’est pas activé dans les configurations officielles du
noyau dans Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3524">CVE-2022-3524</a>

<p>L’outil syzbot a trouvé une situation de compétition dans la pile IPv6 qui
pourrait conduire à une fuite de mémoire. Un utilisateur local pourrait
exploiter cela pour provoquer un déni de service (épuisement de mémoire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3564">CVE-2022-3564</a>

<p>Un défaut a été découvert dans le sous-système Bluetooth L2CAP qui pourrait
aboutir à une utilisation de mémoire après libération. Cela pouvait être
exploitable pour provoquer un déni de service (plantage ou corruption de mémoire)
ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3565">CVE-2022-3565</a>

<p>Un défaut a été découvert dans le pilote mISDN qui pourrait aboutir à
une utilisation de mémoire après libération. Cela peut être exploitable pour
provoquer un déni de service (plantage ou corruption de mémoire) ou éventuellement
pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3594">CVE-2022-3594</a>

<p>Andrew Gaul a signalé que le pilote r8152 Ethernet pourrait journaliser un
nombre excessif de messages dans la réponse aux erreurs de réseau. Un attaquant
distant pourrait éventuellement exploiter cela pour provoquer un déni de service
(épuisement de ressources).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3628">CVE-2022-3628</a>

<p>Dokyung Song, Jisoo Jang et Minsuk Kang ont découvert un potentiel dépassement de
tampon de tas dans le pilote brcmfmac Wi-Fi. Un utilisateur capable de connecter
un périphérique USB malveillant pourrait exploiter cela pour provoquer un déni
de service (plantage ou corruption de mémoire) ou éventuellement pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3640">CVE-2022-3640</a>

<p>Un défaut a été découvert dans le sous-système Bluetooth L2CAP qui pourrait
conduire à une utilisation de mémoire après libération. Cela pourrait être
exploitable pour provoquer un déni de service (plantage ou corruption de
mémoire) ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3643">CVE-2022-3643</a> (XSA-423)

<p>Un défaut a été découvert dans le pilote de dorsal réseau Xen qui pourrait
aboutir à la génération de tampons de paquets mal formés. Si ces paquets étaient
retransmis vers certains autres périphériques réseau, un client Xen pourrait
exploiter cela pour provoquer un déni de service (plantage ou réinitialisation
de périphérique).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4139">CVE-2022-4139</a>

<p>Un défaut a été découvert dans le pilote graphique i915. Pour les GPU gen12
<q>Xe</q>, il échoue à vider les TLB quand c’est nécessaire, aboutissant à ce
que des programmes GPU conservent l’accès à une mémoire libérée. Un utilisateur
local avec accès au GPU pourrait exploiter cela pour divulguer des informations
sensibles, causer un déni de service (plantage ou corruption de mémoire) ou
probablement une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4378">CVE-2022-4378</a>

<p>Kyle Zeng a trouvé un défaut dans procfs qui pourrait provoquer un
dépassement de pile. Un utilisateur local autorisé à écrire sur un sysctl
pourrait utiliser cela pour provoquer un déni de service (plantage ou corruption
de mémoire) ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41849">CVE-2022-41849</a>

<p>Une situation de compétition a été découverte dans le pilote graphique
smscufx qui pourrait conduire à une utilisation de mémoire après
libération. Un utilisateur capable de retirer le périphérique physique tout
en accédant aussi au nœud de périphérique pourrait exploiter cela pour provoquer
un déni de service (plantage ou corruption de mémoire) ou éventuellement pour
une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41850">CVE-2022-41850</a>

<p>Une situation de compétition a été découverte dans le pilote d’entrée
hid-roccat qui pourrait conduire à une utilisation de mémoire après libération.
Un utilisateur local capable d’accéder à un tel périphérique pourrait exploiter
cela pour provoquer un déni de service (plantage ou corruption de mémoire) ou
éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42328">CVE-2022-42328</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-42329">CVE-2022-42329</a> (XSA-424)

<p>Yang Yingliang a signalé que le pilote de dorsal réseau Xen n’utilisait pas
la fonction correcte pour libérer les tampons de paquet dans un cas, ce qui
pourrait conduire à un interblocage. Un client Xen pourrait exploiter cela pour
provoquer un déni de service (blocage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42895">CVE-2022-42895</a>

<p>TamÃ¡s Koczka a signalé un défaut dans le sous-système Bluetooth L2CAP qui
pourrait aboutir à la lecture de mémoire non réinitialisée. Un attaquant proche
capable de se connecter en Bluetooth pourrait exploiter cela pour divulguer des
informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42896">CVE-2022-42896</a>

<p>TamÃ¡s Koczka a signalé des défauts dans le sous-système Bluetooth L2CAP qui
pourraient conduire à une utilisation de mémoire après libération. Un attaquant
proche capable de se connecter en SMP Bluetooth pourrait exploiter cela pour
provoquer un déni de service (plantage ou corruption de mémoire) ou
éventuellement pour une exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47518">CVE-2022-47518</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-47519">CVE-2022-47519</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-47521">CVE-2022-47521</a>

<p>Plusieurs défauts ont été découverts dans le pilote wilc1000 Wi-Fi qui
pourraient conduire à un dépassement de tampon de tas. Un attaquant proche
pourrait les exploiter pour un déni de service (plantage ou corruption de
mémoire) ou éventuellement pour une exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47520">CVE-2022-47520</a>

<p>Un défaut a été découvert dans le pilote wilc1000 Wi-Fi qui pourrait conduire
à un dépassement de tampon de tas. Un utilisateur local ayant la capacité
CAP_NET_ADMIN sur un tel périphérique Wi-Fi pourrait exploiter cela pour un déni
de service (plantage ou corruption de mémoire) ou éventuellement pour une
élévation des privilèges.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 5.10.158-2~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-5.10.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-5.10,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-5.10">\
https://security-tracker.debian.org/tracker/linux-5.10</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3244.data"
# $Id: $
