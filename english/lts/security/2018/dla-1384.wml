<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was found that the open_envvar function in xdg-utils does not
validate strings before launching the program specified by the BROWSER
environment variable, which might allow remote attackers to conduct
argument-injection attacks via a crafted URL.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.0~rc1+git20111210-6+deb7u4.</p>

<p>We recommend that you upgrade your xdg-utils packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1384.data"
# $Id: $
