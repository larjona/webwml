<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>DLA-2836-1 was rolled out, fixing <a href="https://security-tracker.debian.org/tracker/CVE-2021-43527">CVE-2021-43527</a> in nss, but that
lead to a regression, preventing SSL connections in Chromium. The
complete bug report could be found here:
<a href="https://bugs.debian.org/1001219.">https://bugs.debian.org/1001219.</a></p>

<p>For Debian 9 stretch, this problem has been fixed in version
2:3.26.2-1.1+deb9u4.</p>

<p>We recommend that you upgrade your nss packages.</p>

<p>For the detailed security status of nss please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/nss">https://security-tracker.debian.org/tracker/nss</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2836-2.data"
# $Id: $
