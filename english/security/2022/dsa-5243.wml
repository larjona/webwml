<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in lighttpd, a fast webserver
with minimal memory footprint.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37797">CVE-2022-37797</a>

    <p>An invalid HTTP request (websocket handshake) may cause a NULL
    pointer dereference in the wstunnel module.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41556">CVE-2022-41556</a>

    <p>A resource leak in mod_fastcgi and mod_scgi could lead to a denial
    of service after a large number of bad HTTP requests.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 1.4.59-1+deb11u2.</p>

<p>We recommend that you upgrade your lighttpd packages.</p>

<p>For the detailed security status of lighttpd please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/lighttpd">\
https://security-tracker.debian.org/tracker/lighttpd</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5243.data"
# $Id: $
