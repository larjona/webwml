<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential directory-traversal in
<a href="https://www.djangoproject.com/">Django</a>, a popular Python-based web
development framework.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3281">CVE-2021-3281</a></li>
</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:1.10.7-2+deb9u10.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2540.data"
# $Id: $
