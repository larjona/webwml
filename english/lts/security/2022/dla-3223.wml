<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update fixes two file format vulnerabilities in giflib.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11490">CVE-2018-11490</a>

    <p>The <pre>DGifDecompressLine</pre> function in <pre>dgif_lib.c</pre>, as later shipped in
    <pre>cgif.c</pre> in sam2p 0.49.4, has a heap-based buffer overflow because a
    certain "<pre>Private-&gt;RunningCode - 2</pre>" array index is not checked.  This
    will lead to a denial of service or possibly unspecified other
    impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15133">CVE-2019-15133</a>

    <p>A malformed GIF file triggers a divide-by-zero exception in the
    decoder function <pre>DGifSlurp</pre> in <pre>dgif_lib.c</pre> if the <pre>height</pre> field of the
    <pre>ImageSize</pre> data structure is equal to zero.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
5.1.4-3+deb10u1.</p>

<p>We recommend that you upgrade your giflib packages.</p>

<p>For the detailed security status of giflib please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/giflib">https://security-tracker.debian.org/tracker/giflib</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3223.data"
# $Id: $
